﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using Grafy.Representations;

namespace GrafyTests
{
    [TestClass]
    public class IncidencyMatrixTests
    {
        private IncidencyMatrix Matrix { get; set; }

        public IncidencyMatrixTests()
        {
            Matrix = new IncidencyMatrix();
        }

        [TestMethod]
        public void TestMatrixVertex()
        {
            Trace.WriteLine("--Tworzenie wierzcholkow--");

            Matrix.AddNode(3);

            Trace.WriteLine(Matrix.ToString());

            Assert.IsTrue(Matrix.NodeCounter == 3);
        }

        [TestMethod]
        public void TestMatrixEdge()
        {
            TestMatrixVertex();
            Trace.WriteLine("\n--Tworzenie krawedzi--");

            Matrix.AddEdge(0, 1);
            Matrix.AddEdge(0, 2);

            Trace.WriteLine(Matrix.ToString());

            Assert.IsTrue(Matrix.EdgeCounter == 2 && Matrix[0, 0] == 1 && Matrix[0, 1] == 1 && Matrix[1, 0] == 1 && Matrix[1, 2] == 1);
        }

    }
}