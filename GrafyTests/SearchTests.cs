﻿using System.Diagnostics;
using System.Threading.Tasks;
using Grafy.Functions;
using Grafy.Representations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GrafyTests
{
    [TestClass]
    public class SearchTests
    {
        private readonly AdjacencyList _list = new AdjacencyList();

        public SearchTests()
        {
            _list.AddNode(3);
            
            _list.AddEdge(0,1);
            _list.AddEdge(1,2);
            _list.AddEdge(0,2);
        }

        [TestMethod]
        public void SearchBreadthFirstTest()
        {
            var search = new Search<AdjacencyList>(_list);
            Task.Run(async () =>
            {
                await search.BFSAsync();
            }).GetAwaiter().GetResult();
           

            Trace.WriteLine(search.ToString());

            Assert.IsTrue(search.Sequence != null);
        }

        [TestMethod]
        public void SearchDepthFirstTest()
        {
            var search = new Search<AdjacencyList>(_list);
            
            Task.Run(async() =>
            {
                await search.DFSAsync();
            }).GetAwaiter().GetResult();
            

            Trace.WriteLine(search.ToString());

            Assert.IsTrue(search.Sequence != null);
        }
    }
}
