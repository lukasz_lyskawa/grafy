﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using Grafy.Representations;

namespace GrafyTests
{
    [TestClass]
    public class AdjacencyListTests
    {
        private readonly AdjacencyList _list;

        public AdjacencyListTests()
        {
            _list = new AdjacencyList();
        }

        [TestMethod]
        public void AddVertexTest()
        {
            int AMOUNT = 3;
            Trace.WriteLine("--Tworze 3 wierzcholki--");

            _list.AddNode(AMOUNT);

            Trace.WriteLine(_list.ToString());

            Assert.IsTrue(_list.NodeCounter == AMOUNT);

        }


        [TestMethod]
        public void AddEdgeTest()
        {
            AddVertexTest();
            Trace.WriteLine("--Tworze krawedzie--");

            _list.AddEdge(0, 1);
            _list.AddEdge(1, 2);
            _list.AddEdge(0, 2);

            Trace.WriteLine(_list.ToString());

            Assert.IsTrue(_list.EdgeCounter == 3 && _list[0, 0] == 1 && _list[1, 0] == 0 && _list[0, 1] == 2 && _list[2, 0] == 0);
        }
    }
}