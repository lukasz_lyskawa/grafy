﻿using System.Diagnostics;
using Grafy.Representations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GrafyTests
{
    [TestClass]
    public class OrientedAdjacencyListTests
    {
        private readonly OrientedAdjacencyList _list;

        public OrientedAdjacencyListTests()
        {
            _list = new OrientedAdjacencyList();
        }

        [TestMethod]
        public void TransposeTest()
        {
            int AMOUNT = 3;
            Trace.WriteLine("--Tworze 3 wierzcholki--");

            _list.AddNode(AMOUNT);

            Trace.WriteLine("--Tworze 3 krawedzie--");

            _list.AddEdge(0,1);
            _list.AddEdge(1,2);
            _list.AddEdge(0,2);

            Trace.WriteLine(_list.ToString());

            Trace.WriteLine("--Transpozycja--");

            var trans = _list.Transpose();

            Trace.WriteLine(_list.Transpose().ToString());


            Assert.IsTrue(_list.NodeCounter == AMOUNT);
        }
    }
}
