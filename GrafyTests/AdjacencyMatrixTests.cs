﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Grafy.Representations;
using System.Diagnostics;

namespace GrafyTests
{
    [TestClass]
    public class AdjacencyMatrixTests
    {
        private AdjacencyMatrix _matrix;
        public AdjacencyMatrixTests()
        {
            _matrix = new AdjacencyMatrix();
        }

        [TestMethod]
        public void AddNodeTest()
        {
            _matrix.AddNode();
            _matrix.AddNode();
            Assert.IsTrue(_matrix.Matrix.Size().Item1==2 && _matrix.Matrix.Size().Item2==2);
        }
        [TestMethod]
        public void MergeNodesTest()
        {
            _matrix.AddNode(5);
            _matrix.AddEdge(0,1);
            _matrix.AddEdge(1,2);
            _matrix.AddEdge(2,3);


            var merged = _matrix.MergeNodes(4, 1);

            Trace.WriteLine($"-----Graf-----\n" +
                            $"{_matrix}\n" +
                            $"--Merge(0,4)--\n" +
                            $"{merged}");
            merged = merged.MergeNodes(3, 0);

            Assert.IsTrue(true);
        }
    }
}