﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Grafy.Functions;
using System.Diagnostics;
using System.Linq;
using Grafy.Representations;

namespace Grafy.Functions.Tests
{
    [TestClass()]
    public class EulerTests
    {
        [TestMethod()]
        public void OutDegreeTest()
        {

            var test = new OrientedAdjacencyMatrix(3);
            test.AddEdge(0,1);
            test.AddEdge(1,2);
            test.AddEdge(2,0);

            Euler e = new Euler(test);
            Trace.WriteLine(e.OutDegree(0));
            Trace.WriteLine(e.InDegree(0));
            Trace.WriteLine(e.OutDegree(1));
            Trace.WriteLine(e.InDegree(1));
            Trace.WriteLine(e.OutDegree(2));
            Trace.WriteLine(e.InDegree(2));
            Trace.WriteLine(test.ToString());
            bool result=true;
            for (int i = 0; i < test.NodeCounter; i++)
            {
                result = result && e.OutDegree(i) == e.InDegree(i);
            }
            Assert.IsTrue(result);
        }
        [TestMethod()]
        public void FindOrientedEulerCircuitTest()
        {
            var test = new OrientedAdjacencyMatrix(3);
            test.AddEdge(0, 1);
            test.AddEdge(1, 2);
            test.AddEdge(2, 0);

            Euler e = new Euler(test);
            var result = e.FindEulerCircuit();
            foreach (var i in result)
            {
                Trace.WriteLine(i);
            }
            Assert.IsTrue(result.Any());
        }
    }
}

namespace Grafy.FunctionsTests
{
    [TestClass()]
    public class EulerTests
    {
        [TestMethod]
        public void FindEulerCircuitTest()
        {
            var testmatrix = new AdjacencyMatrix(3);
            testmatrix.AddEdge(0,1);
            testmatrix.AddEdge(1,2);
            testmatrix.AddEdge(2,0);
            Trace.WriteLine(testmatrix.ToString());
            Euler e = new Euler(testmatrix);
            var result = e.FindEulerCircuit();
            foreach (var i in result)
            {
                Trace.WriteLine(i);
            }
            Assert.IsTrue(result.Any());
        }
    }
}