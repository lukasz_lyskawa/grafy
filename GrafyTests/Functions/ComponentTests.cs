﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Grafy.Functions;
using System.Diagnostics;
using Grafy.Representations;

namespace Grafy.FunctionsTests
{
    [TestClass]
    public class ComponentTests
    {
        [TestMethod]
        public void ComputeAsyncTest()
        {
            var am = new AdjacencyMatrix(10);

            am.AddEdge(0, 2);
            am.AddEdge(2, 9);
            am.AddEdge(0, 9);
            am.AddEdge(8, 9);
            am.AddEdge(8, 4);
            am.AddEdge(8, 6);
            am.AddEdge(4, 6);

            am.AddEdge(3, 5);
            am.AddEdge(3, 1);
            am.AddEdge(5, 1);
            am.AddEdge(7, 1);

            Trace.WriteLine(am.ToString());
            var component = new Component<AdjacencyMatrix>(am);
            var componentsNumber = component.ComputeAsync().Result;
            Assert.IsTrue(componentsNumber==2);
        }
    }
}