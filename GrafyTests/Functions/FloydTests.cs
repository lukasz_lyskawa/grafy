﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Grafy.Functions;
using System.Diagnostics;
using Grafy.Representations;

namespace Grafy.FunctionsTests
{
    [TestClass]
    public class FloydTests
    {

        [TestMethod]
        public void ClosestDistanceBetweenTest()
        {
            var testmatrix = new WeightedAdjacencyMatrix(5);
            testmatrix.AddEdge(0, 1, 2);
            testmatrix.AddEdge(1, 2, 1);
            testmatrix.AddEdge(2, 3, 2);
            testmatrix.AddEdge(3, 4, 10);
            testmatrix.AddEdge(4, 0, 1);
            testmatrix.AddEdge(1, 4, 8);
            testmatrix.AddEdge(1, 3, 4);
            var f = new Floyd(testmatrix);
            Trace.WriteLine(f._floydResult);
            Assert.IsTrue(f.ClosestDistanceBetween(0, 3) == 5);
        }

        [TestMethod]
        public void FloydTest()
        {
            var testmatrix = new WeightedAdjacencyMatrix(5);
            testmatrix.AddEdge(0, 1, 2);
            testmatrix.AddEdge(1, 2, 1);
            testmatrix.AddEdge(2, 3, 2);
            testmatrix.AddEdge(3, 4, 10);
            testmatrix.AddEdge(4, 0, 1);
            testmatrix.AddEdge(1, 4, 8);
            testmatrix.AddEdge(1, 3, 4);
            var f = new Floyd(testmatrix);
            Trace.WriteLine(f._floydResult);
            Trace.WriteLine(f.Diameter);
            Trace.WriteLine(f.Radius);
            var l = f.CentralVertices;
            foreach (var i in l)
            {
                Trace.Write(i+", ");
            }
            Assert.Inconclusive();
        }
    }
}