﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Grafy.Representations;
using System.Diagnostics;

namespace GrafyTests
{
    [TestClass]
    public class OrientedAdjacencyMatrixTests
    {
        private OrientedAdjacencyMatrix _testGraph;
        public OrientedAdjacencyMatrixTests()
        {
            _testGraph = new OrientedAdjacencyMatrix(5);
            _testGraph.AddEdge(0,1);
            _testGraph.AddEdge(2,4);
            _testGraph.AddEdge(4,2);

        }
        [TestMethod]
        public void TransposeTest()
        {
            var transposed = _testGraph.Transpose();
            Trace.WriteLine($"------Graph-----\n" +
                            $"{_testGraph}\n" +
                            $"---Transposed---\n" +
                            $"{transposed}");

            bool testResult = true;
            for (int i = 0; i < _testGraph.NodeCounter; i++)
            {
                for (int j = 0; j < _testGraph.NodeCounter; j++)
                {
                    if (_testGraph[i, j] != transposed[j, i])
                    {
                        testResult = false;
                    }
                }
            }

            Assert.IsTrue(testResult);
        }
    }
}