﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grafy.Functions;
using Grafy.Interface;
using Grafy.Representations;

namespace Grafy
{
    public class WeightedOrientedGraph : IGraph, IWeightedGraph
    {
        public WeightedOrientedAdjacencyMatrix AdjacencyMatrix = new WeightedOrientedAdjacencyMatrix();
        public OrientedAdjacencyList AdjacencyList = new OrientedAdjacencyList();
        public IncidencyMatrix IncidencyMatrix = new IncidencyMatrix();

        private bool _graphChanged = true;
        private Floyd _floyd;

        public async Task<bool> IsConnected()
        {
            var componentOperation = new Component<WeightedOrientedAdjacencyMatrix>(AdjacencyMatrix);
            await componentOperation.ComputeAsync();
            return componentOperation.IsConnected != null && componentOperation.IsConnected.Value;
        }

        public async Task<int> Components()
        {
            var componentOperation = new Component<WeightedOrientedAdjacencyMatrix>(AdjacencyMatrix);
            return await componentOperation.ComputeAsync();
        }

        public void AddVertex(int count = 1)
        {
            AdjacencyMatrix.AddNode(count);
            AdjacencyList.AddNode(count);
            IncidencyMatrix.AddNode(count);
        }

        public void RemoveVertex(int vertex)
        {
            AdjacencyMatrix.RemoveNode(vertex);
            AdjacencyList.RemoveNode(vertex);
            IncidencyMatrix.RemoveNode(vertex);
        }

        public void AddEdge(int vertex1, int vertex2, int weight)
        {
            AdjacencyMatrix.AddEdge(vertex1, vertex2, weight);
            AdjacencyList.AddEdge(vertex1, vertex2);
            IncidencyMatrix.AddEdge(vertex1, vertex2);
        }
        [Obsolete("Use AddEdge(x,y,weight) instead.", true)]
        public void AddEdge(int vertex1, int vertex2)
        {
            throw new NotImplementedException();
        }

        public void RemoveEdge(int vertex1, int vertex2)
        {
            AdjacencyMatrix.RemoveEdge(vertex1, vertex2);
            AdjacencyList.RemoveEdge(vertex1, vertex2);
            IncidencyMatrix.RemoveEdge(vertex1, vertex2);
        }

        public void MergeVertices(int vertex1, int vertex2)
        {
            AdjacencyMatrix = new WeightedOrientedAdjacencyMatrix((AdjacencyMatrix)AdjacencyMatrix.MergeNodes(vertex1, vertex2));
            AdjacencyList = new OrientedAdjacencyList((AdjacencyList)AdjacencyList.MergeNodes(vertex1, vertex2));
            IncidencyMatrix = (IncidencyMatrix)IncidencyMatrix.MergeNodes(vertex1, vertex2);
        }

        public async Task<Search<AdjacencyList>> SearchAsync(int startNode, SearchType searchType)
        {
            var search = new Search<AdjacencyList>(AdjacencyList);
            if (searchType == SearchType.BFS)
            {
                await search.BFSAsync(startNode);
            }
            else
            {
                await search.DFSAsync(startNode);
            }
            return search;
        }

        public int Diameter
        {
            get
            {
                if (_graphChanged)
                {
                    _floyd = new Floyd(AdjacencyMatrix);
                    _graphChanged = false;
                }

                return _floyd.Diameter;
            }
        }

        public int Radius
        {
            get
            {
                if (_graphChanged)
                {
                    _floyd = new Floyd(AdjacencyMatrix);
                    _graphChanged = false;
                }
                return _floyd.Radius;
            }
        }

        public List<int> CentralVertices
        {
            get
            {
                if (_graphChanged)
                {
                    _floyd = new Floyd(AdjacencyMatrix);
                    _graphChanged = false;
                }
                return _floyd.CentralVertices;
            }
        }

        public int DistanceBetweenVertices(int vertex1, int vertex2)
        {
            if (_graphChanged)
            {
                _floyd = new Floyd(AdjacencyMatrix);
                _graphChanged = false;
            }

            return _floyd.ClosestDistanceBetween(vertex1, vertex2);
        }
    }


}
