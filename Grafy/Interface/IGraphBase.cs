﻿using System.Threading.Tasks;

namespace Grafy.Interface
{
    public interface IGraphBase
    {
        Task<bool> IsConnected();
        Task<int> Components();

        void AddVertex(int count=1);

        void RemoveVertex(int vertex);

        void AddEdge(int vertex1, int vertex2);

        void RemoveEdge(int vertex1, int vertex2);

        void MergeVertices(int vertex1, int vertex2);

        
    }
}
