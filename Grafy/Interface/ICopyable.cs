﻿namespace Grafy.Interface
{
    public interface ICopyable
    {
       ICopyable DeepCopy();
    }
}
