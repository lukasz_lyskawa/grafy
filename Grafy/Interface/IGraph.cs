﻿using System.Threading.Tasks;
using Grafy.Functions;
using Grafy.Representations;

namespace Grafy.Interface
{
    public interface IGraph : IGraphBase
    {
        Task<Search<AdjacencyList>> SearchAsync(int startNode, SearchType searchType);
    }
}
