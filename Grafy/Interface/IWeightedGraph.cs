﻿using System.Collections.Generic;

namespace Grafy.Interface
{
    interface IWeightedGraph:IGraphBase
    {
        int Diameter { get; }
        int Radius { get; }
        List<int> CentralVertices { get; } 

        int DistanceBetweenVertices(int vertex1, int vertex2);

        void AddEdge(int vertex1, int vertex2,int weight);


    }
}
