﻿namespace Grafy.Interface
{
    public interface IGraphRepresentation
    {
        int this[int x, int y] { get; }

        int EdgeCounter { get; }

        int NodeCounter { get; }

        void AddNode(int amount=1);

        void RemoveNode(int nodeIndex);

        void AddEdge(int node1, int node2);

        void RemoveEdge(int node1,int node2);

        IGraphRepresentation Transpose();

        IGraphRepresentation MergeNodes(int x, int y);

        string ToString();
    }
}
