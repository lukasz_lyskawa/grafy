﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Grafy.Representations;

namespace Grafy.Functions
{
    public static class MST
    {
        public static async Task<WeightedAdjacencyMatrix> Prim(this WeightedAdjacencyMatrix graph, int startVertex)
        {
            return await Task.Run(() =>
            {
                var graphcopy = (WeightedAdjacencyMatrix)graph.DeepCopy();

                for (int i = 0; i < graphcopy.NodeCounter; i++)
                {
                    for (int j = 0; j < graphcopy.NodeCounter; j++)
                    {
                        if (graphcopy[i, j] == 0)
                        {
                            graphcopy[i, j] = int.MaxValue;
                        }
                    }
                }
                var visited = new List<bool>();
                for (int i = 0; i < graphcopy.NodeCounter; i++)
                {
                    visited.Add(false);
                }
                visited[startVertex] = true;
                var counter = 1;
                var closest = new int[graphcopy.NodeCounter];
                var weight = new int[graphcopy.NodeCounter];
                for (int i = 0; i < graphcopy.NodeCounter; i++)
                {
                    weight[i] = graphcopy[i, startVertex];
                    if (weight[i] > 0 && weight[i] != int.MaxValue)
                    {
                        closest[i] = startVertex;
                    }
                }

                var result = new WeightedAdjacencyMatrix(graphcopy.NodeCounter);

                do
                {
                    var minWeight = int.MaxValue;
                    var minVertex = 0;
                    for (int i = 0; i < graphcopy.NodeCounter; i++)
                    {
                        if (visited[i]) continue;

                        if (weight[i] < minWeight)
                        {
                            minWeight = weight[i];
                            minVertex = i;
                        }
                    }
                    if (minWeight == int.MaxValue) throw new ArgumentException("Prim: Graph is not connected.");
                    visited[minVertex] = true;
                    counter++;
                    result.AddEdge(minVertex, closest[minVertex], minWeight);

                    for (int i = 0; i < graphcopy.NodeCounter; i++)
                    {
                        if (visited[i]) continue;
                        if (graphcopy.IsNeighbour(i, minVertex))
                        {
                            if (graphcopy[i, minVertex] < weight[i])
                            {
                                weight[i] = graphcopy[i, minVertex];
                                closest[i] = minVertex;
                            }
                        }
                    }
                } while (counter != graphcopy.NodeCounter);

                return result;
            });
        }

        private static bool IsNeighbour(this WeightedAdjacencyMatrix matrix, int v1, int v2)
        {
            return (matrix[v1, v2] > 0 && matrix[v1, v2] != int.MaxValue);
        }

        public static async Task<WeightedOrientedAdjacencyMatrix> Prim(this WeightedOrientedAdjacencyMatrix graph, int startVertex)
        {
            return await Task.Run(() =>
            {
                var graphcopy = (WeightedOrientedAdjacencyMatrix)graph.DeepCopy();

                for (int i = 0; i < graphcopy.NodeCounter; i++)
                {
                    for (int j = 0; j < graphcopy.NodeCounter; j++)
                    {
                        if (graphcopy[i, j] == 0)
                        {
                            graphcopy[i, j] = int.MaxValue;
                        }
                    }
                }
                var visited = new List<bool>();
                for (int i = 0; i < graphcopy.NodeCounter; i++)
                {
                    visited.Add(false);
                }
                visited[startVertex] = true;
                var counter = 1;
                var closest = new int[graphcopy.NodeCounter];
                var weight = new int[graphcopy.NodeCounter];
                for (int i = 0; i < graphcopy.NodeCounter; i++)
                {
                    weight[i] = graphcopy[i, startVertex];
                    if (weight[i] > 0 && weight[i] != int.MaxValue)
                    {
                        closest[i] = startVertex;
                    }
                }

                var result = new WeightedOrientedAdjacencyMatrix(graphcopy.NodeCounter);

                do
                {
                    var minWeight = int.MaxValue;
                    var minVertex = 0;
                    for (int i = 0; i < graphcopy.NodeCounter; i++)
                    {
                        if (visited[i]) continue;

                        if (weight[i] < minWeight)
                        {
                            minWeight = weight[i];
                            minVertex = i;
                        }
                    }
                    if (minWeight == int.MaxValue) throw new ArgumentException("Prim: Graph is not connected.");
                    visited[minVertex] = true;
                    counter++;
                    result.AddEdge(minVertex, closest[minVertex], minWeight);

                    for (int i = 0; i < graphcopy.NodeCounter; i++)
                    {
                        if (visited[i]) continue;
                        if (graphcopy.IsNeighbour(i, minVertex))
                        {
                            if (graphcopy[i, minVertex] < weight[i])
                            {
                                weight[i] = graphcopy[i, minVertex];
                                closest[i] = minVertex;
                            }
                        }
                    }
                } while (counter != graphcopy.NodeCounter);

                return result;
            });
        }

        private static bool IsNeighbour(this WeightedOrientedAdjacencyMatrix matrix, int v1, int v2)
        {
            return (matrix[v1, v2] > 0 && matrix[v1, v2] != int.MaxValue);
        }

    }
}
