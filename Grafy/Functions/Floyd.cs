﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Grafy.Interface;
using Grafy.Representations;

namespace Grafy.Functions
{
    public class Floyd
    {
        public WeightedAdjacencyMatrix _floydResult;
        private WeightedAdjacencyMatrix _dist;
        public Floyd(WeightedAdjacencyMatrix matrix)
        {
            _dist = new WeightedAdjacencyMatrix((AdjacencyMatrix)matrix.DeepCopy());
            Run();           
        }

        public Floyd(WeightedOrientedAdjacencyMatrix matrix)
        {
            _dist = new WeightedAdjacencyMatrix((AdjacencyMatrix)matrix.DeepCopy());
            Run();
        }

        private void Run()
        {
            for (int i = 0; i < _dist.NodeCounter; i++)
            {
                for (int j = 0; j < _dist.NodeCounter; j++)
                {
                    if (i == j) continue;
                    if (_dist[i, j] == 0)
                    {
                        _dist[i, j] = int.MaxValue;
                    }
                }
            }

            for (int pivot = 0; pivot < _dist.NodeCounter; pivot++)
            {
                for (int vertex1 = 0; vertex1 < _dist.NodeCounter; vertex1++)
                {
                    for (int vertex2 = 0; vertex2 < _dist.NodeCounter; vertex2++)
                    {
                        if (_dist[vertex1, vertex2] > (_dist[vertex1, pivot] + _dist[pivot, vertex2]))
                        {
                            if (_dist[vertex1, pivot] == int.MaxValue || _dist[pivot, vertex2] == int.MaxValue)
                            {
                                continue;
                            }
                            _dist[vertex1, vertex2] = _dist[vertex1, pivot] + _dist[pivot, vertex2];
                        }
                    }
                }
            }
            _floydResult = _dist;
        }

        //srednia max
        //promien min z najwiekszych
        //centrum max==promien

        public int Diameter
        {
            get
            {
                var diam = 0;
                for (int i = 0; i < _floydResult.NodeCounter; i++)
                {
                    for (int j = 0; j < _floydResult.NodeCounter; j++)
                    {
                        if(i==j) continue;
                        if (diam < _floydResult.Matrix[i, j])
                        {
                            diam = _floydResult.Matrix[i, j];
                        }
                    }
                }
                return (diam == 0) ? Int32.MaxValue : diam;
            }
        }

        public int Radius
        {
            get
            {
                var min = int.MaxValue;
                for (int i = 0; i < _floydResult.NodeCounter; i++)
                {
                    var max = 0;
                    for (int j = 0; j < _floydResult.NodeCounter; j++)
                    {
                        if (i == j) continue;
                        if (max < _floydResult.Matrix[i, j])
                        {
                            max = _floydResult.Matrix[i, j];
                        }
                    }
                    if (max < min)
                    {
                        min = max;
                    }
                }
                Debug.WriteLine(_floydResult.ToString());
                return min;
            }
        }
        public int ClosestDistanceBetween(int from, int to)
        {
            return _floydResult[from, to];
        }

        public List<int> CentralVertices
        {
            get
            {
                var radius = Radius;
                var list = new List<int>();
                for (int i = 0; i < _floydResult.NodeCounter; i++)
                {
                    var max = 0;
                    for (int j = 0; j < _floydResult.NodeCounter; j++)
                    {
                        
                        if (i == j) continue;
                        
                        if (_floydResult.Matrix[i, j]>max)
                        {
                            max = _floydResult.Matrix[i, j];
                        }

                    }
                    if (max == radius)
                    {
                        list.Add(i);
                    }
                }
                return list;
            }
        }


    }
}
