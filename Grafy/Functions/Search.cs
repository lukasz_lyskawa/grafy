﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grafy.Interface;
using Grafy.Representations;

namespace Grafy.Functions
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T">For now only works with AdjacencyList</typeparam>
    public class Search<T> where T : IGraphRepresentation, new()
    {
        public List<int> Path { get; } 
        public int[] Sequence { get; }
        public T Tree { get; }
        public T Remains { get; }

        private readonly AdjacencyList _graph;

        private int _sequenceCounter = 1;

        private int _locked;

        private string algorithm = "";

        public Search(AdjacencyList graph)
        {
            Path=new List<int>();

            Sequence = new int[graph.NodeCounter];

            Tree = (T)Activator.CreateInstance(typeof(T), graph.NodeCounter);

            Remains = (T)Activator.CreateInstance(typeof(T), graph.NodeCounter);

            _graph = graph;
        }


        public async Task BFSAsync(int startAtVertex = 0)
        {
            if (_locked == 0)
            {
                _locked = 1;
                algorithm = "BFS";

                await Task.Run(() =>
                {
                    var queue = new Queue<int>();
                    queue.Enqueue(startAtVertex);

                    Sequence[startAtVertex] = 1;

                    int sequenceCounter = 1;

                    Path.Add(startAtVertex);


                    while (queue.Count > 0)
                    {
                        var currentVertex = queue.Dequeue();

                        for (int i = 0; i < _graph.List[currentVertex].Count; i++)
                        {
                            var adjacentVertex = _graph.List[currentVertex].ElementAt(i);

                            if (Sequence[adjacentVertex] == 0)
                            {
                                Sequence[adjacentVertex] = ++sequenceCounter;

                                Tree.AddEdge(currentVertex, adjacentVertex);

                                Path.Add(adjacentVertex);

                                queue.Enqueue(adjacentVertex);
                            }
                            else if (Sequence[currentVertex] < Sequence[adjacentVertex])
                            {
                                Remains.AddEdge(currentVertex, adjacentVertex);
                            }
                        }
                    }
                });

                _locked = 0;
            }
            else
            {
                throw new AccessViolationException("Only one search function is allowed to be working at a time per Search object instance.");
            }


        }

        public async Task DFSAsync(int startAtVertex = 0)
        {
            if (_locked == 0 || _locked==2)
            {
                _locked = 2;
                algorithm = "DFS";

                await Task.Run(async () =>
                {

                    Sequence[startAtVertex] = 1;

                    Path.Add(startAtVertex);

                    var currentVertex = startAtVertex;


                    foreach (var adjacentVertex in _graph.List[currentVertex])
                    {
                        if (Sequence[adjacentVertex] == 0)
                        {
                            Sequence[adjacentVertex] = ++_sequenceCounter;

                            Tree.AddEdge(currentVertex, adjacentVertex);

                            await DFSAsync(adjacentVertex);

                        }
                        else if (Sequence[currentVertex] < Sequence[adjacentVertex])
                        {
                            Remains.AddEdge(currentVertex, adjacentVertex);
                        }
                    }
                });

                _locked = 0;
            }
            else
            {
                throw new AccessViolationException("Only one search function is allowed to be working at a time per Search object instance.");
            }


        }

        public override string ToString()
        {
            var result = $"---{algorithm}---\nSequence\n";

            for (int index = 0; index < Sequence.Length; index++)
            {
                result += $"{index}: {Sequence[index]}\n";
            }

            result += $"\nTree\n{Tree}\n";

            result+=$"\nRemains\n{Remains}\n";

            return result;
        }
    }
}
