﻿using System.Collections.Generic;
using Grafy.Interface;
using Grafy.Representations;

namespace Grafy.Functions
{
    public class Euler
    {
        private readonly Stack<int> _pathStack = new Stack<int>();
        private readonly List<int> _finalPath = new List<int>();
        private readonly bool _oriented;
        private readonly AdjacencyMatrix _matrix;
        public Euler(AdjacencyMatrix matrix)
        {
            _matrix = (AdjacencyMatrix)matrix.DeepCopy();
            _oriented = _matrix is IOriented;
        }

        public int GetDegree(int vertex)
        {
            int deg=0;
            for (int i = 0; i < _matrix.NodeCounter; i++)
            {
                if (_matrix[vertex, i] > 0)
                {
                    deg++;
                }
            }
            return deg;
        }

        public int OutDegree(int vertex)
        {
            int deg = 0;
            for (int i = 0; i < _matrix.NodeCounter; i++)
            {
                //if(i<vertex) continue;
                if (_matrix[vertex, i] > 0)
                {
                    deg++;
                }
            }
            return deg;
        }

        public int InDegree(int vertex)
        {
            int deg = 0;
            for (int i = 0; i < _matrix.NodeCounter; i++)
            {
                //if(i<vertex) continue;
                if (_matrix[i, vertex] > 0)
                {
                    deg++;
                }
            }
            return deg;
        }

        public int FindRoot()
        {
            int root = 0;
            int unevenDegCount = 0;
            for (int i = 0; i < _matrix.NodeCounter; i++)
            {
                if (GetDegree(i) % 2 != 0 && !_oriented)
                {
                    unevenDegCount++;
                    root = i;
                }
                if (InDegree(i) != OutDegree(i) && _oriented)
                {
                    unevenDegCount++;
                    root = i;
                }
            }
            if (unevenDegCount != 0)
            {
                return -1;
            }
            else
            {
                return root;
            }
        }

        public bool AllVisited(int vertex)
        {
            for (int i = 0; i < _matrix.NodeCounter; i++)
            {
                //if(i<vertex && _oriented) continue;
                if (_matrix[vertex, i] > 0)
                    return false;
            }
            return true;
        }

        public void FindEuler(int root)
        {
            int ind;
            _pathStack.Clear();
            _pathStack.Push(root);
            while (_pathStack.Count > 0)
            {
                ind = _pathStack.Peek();

                if (AllVisited(ind))
                {
                    _finalPath.Add(_pathStack.Pop());
                }

                else
                {
                    for (int i = 0; i < _matrix.NodeCounter; i++)
                    {
                        if (_matrix[ind, i] > 0)
                        {
                            _matrix[ind, i] = 0;
                            _matrix[i, ind] = 0;
                            _pathStack.Push(i);
                            break;
                        }
                    }
                }
            }
        }

        public List<int> FindEulerCircuit()
        {
            int root = FindRoot();

            if (root > -1)
            {
                FindEuler(root);
                return _finalPath;
            }
            return new List<int>();
        }
    }
}
