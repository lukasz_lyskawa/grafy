﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Grafy.Interface;
using Grafy.Representations;

namespace Grafy.Functions
{
    public class Component<T> where T : IGraphRepresentation, ICopyable, new()
    {
        private T _graph;

        public bool? IsConnected { get; private set; }

        public Component(T graph)
        {
            _graph = (T)graph.DeepCopy();
            IsConnected = null;
        }

        public async Task<int> ComputeAsync()
        {
            if (_graph is IOriented)
            {
                return await ComputeOrientedAsync();
            }
            else
            {
                return await ComputeNormalAsync();
            }
        }

        private async Task<int> ComputeNormalAsync()
        {
            return await Task.Run(() =>
            {
                if (_graph.NodeCounter == 0) return 0;

                int result = 0;

                if (_graph == null)
                {
                    return 0;
                }

                bool stop = false;
                do
                {
                    for (int i = 0; i < _graph.NodeCounter; i++)
                    {

                        if (_graph[0, i] > 0)
                        {
                            _graph = (T)_graph.MergeNodes(0, i);
                        }
                        if (i == _graph.NodeCounter - 1 && _graph[0, _graph.NodeCounter - 1] == 0)
                        {
                            result++;
                            _graph.RemoveNode(0);
                        }
                        if (_graph.NodeCounter == 0)
                        {
                            stop = true;
                        }
                    }
                } while (!stop);

                IsConnected = result == 1;

                return result;
            });

        }
        private async Task<int> ComputeOrientedAsync()
        {
            return await Task.Run(() =>
            {
                if (_graph.NodeCounter == 0) return 0;

                int result = 0;

                if (_graph == null)
                {
                    return 0;
                }

                bool stop = false;
                do
                {
                    for (int i = 0; i < _graph.NodeCounter; i++)
                    {

                        Debug.WriteLine(_graph[0, i] + " " + _graph[i, 0] + " :i\n" + _graph.ToString());
                        if (_graph[0, i] > 0 || _graph[i, 0] > 0)
                        {
                            _graph = (T)_graph.MergeNodes(0, i);
                        }
                        Debug.WriteLine(i == _graph.NodeCounter - 1 && _graph[0, _graph.NodeCounter - 1] == 0);
                        if (i == _graph.NodeCounter - 1 && _graph[0, _graph.NodeCounter - 1] == 0 && _graph[_graph.NodeCounter - 1, 0] == 0)
                        {
                            result++;
                            _graph.RemoveNode(0);
                        }
                        if (_graph.NodeCounter == 0)
                        {
                            stop = true;
                        }
                    }
                } while (!stop);


                IsConnected = result == 1;

                return result;
            });

        }

    }
}
