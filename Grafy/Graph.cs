﻿using System.Threading.Tasks;
using Grafy.Functions;
using Grafy.Interface;
using Grafy.Representations;

namespace Grafy
{
    public class Graph : IGraph
    {
        public AdjacencyList AdjacencyList = new AdjacencyList();
        public AdjacencyMatrix AdjacencyMatrix = new AdjacencyMatrix();
        public IncidencyMatrix IncidencyMatrix = new IncidencyMatrix();

        public virtual async Task<bool> IsConnected()
        {
            var componentOperation = new Component<AdjacencyMatrix>(AdjacencyMatrix);
            await componentOperation.ComputeAsync();
            return componentOperation.IsConnected != null && componentOperation.IsConnected.Value;
        }

        public virtual async Task<int> Components()
        {
            var componentOperation = new Component<AdjacencyMatrix>(AdjacencyMatrix);
            return await componentOperation.ComputeAsync();
        }


        public virtual void AddVertex(int count=1)
        {
            for (int i = 0; i < count; i++)
            {
                AdjacencyMatrix.AddNode();
                AdjacencyList.AddNode();
                IncidencyMatrix.AddNode();
            }
           
        }

        public virtual void RemoveVertex(int vertex)
        {
            AdjacencyMatrix.RemoveNode(vertex);
            AdjacencyList.RemoveNode(vertex);
            IncidencyMatrix.RemoveNode(vertex);
        }

        public virtual void AddEdge(int vertex1, int vertex2)
        {
            AdjacencyMatrix.AddEdge(vertex1, vertex2);
            AdjacencyList.AddEdge(vertex1, vertex2);
            IncidencyMatrix.AddEdge(vertex1, vertex2);
        }

        public virtual void RemoveEdge(int vertex1, int vertex2)
        {
            AdjacencyMatrix.RemoveEdge(vertex1, vertex2);
            AdjacencyList.RemoveEdge(vertex1, vertex2);
            IncidencyMatrix.RemoveEdge(vertex1, vertex2);
        }

        public virtual void MergeVertices(int vertex1, int vertex2)
        {
            AdjacencyMatrix = (AdjacencyMatrix)AdjacencyMatrix.MergeNodes(vertex1, vertex2);
            AdjacencyList = (AdjacencyList)AdjacencyList.MergeNodes(vertex1, vertex2);
            IncidencyMatrix = (IncidencyMatrix)IncidencyMatrix.MergeNodes(vertex1, vertex2);
        }

        public async Task<Search<AdjacencyList>> SearchAsync(int startNode, SearchType searchType)
        {
            var search = new Search<AdjacencyList>(AdjacencyList);
            if (searchType == SearchType.BFS)
            {
                await search.BFSAsync(startNode);
            }
            else
            {
                await search.DFSAsync(startNode);
            }
            return search;
        }
    }

    public enum SearchType
    {
        DFS, BFS
    }
}
