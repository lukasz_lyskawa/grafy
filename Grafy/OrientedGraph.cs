﻿using System;
using System.Threading.Tasks;
using Grafy.Functions;
using Grafy.Interface;
using Grafy.Representations;

namespace Grafy
{
    public class OrientedGraph : IGraph
    {
        public OrientedAdjacencyList AdjacencyList = new OrientedAdjacencyList();
        public OrientedAdjacencyMatrix AdjacencyMatrix = new OrientedAdjacencyMatrix();
        public IncidencyMatrix IncidencyMatrix = new IncidencyMatrix();



        public async Task<bool> IsConnected()
        {
            var componentOperation = new Component<OrientedAdjacencyMatrix>(AdjacencyMatrix);
            await componentOperation.ComputeAsync();
            return componentOperation.IsConnected != null && componentOperation.IsConnected.Value;
        }

        public async Task<int> Components()
        {
            var componentOperation = new Component<OrientedAdjacencyMatrix>(AdjacencyMatrix);
            return await componentOperation.ComputeAsync();
        }

        public void AddVertex(int count = 1)
        {
            AdjacencyMatrix.AddNode(count);
            AdjacencyList.AddNode(count);
            IncidencyMatrix.AddNode(count);
        }

        public void RemoveVertex(int vertex)
        {
            AdjacencyMatrix.RemoveNode(vertex);
            AdjacencyList.RemoveNode(vertex);
            IncidencyMatrix.RemoveNode(vertex);
        }

        public void AddEdge(int vertex1, int vertex2)
        {
            AdjacencyMatrix.AddEdge(vertex1,vertex2);
            AdjacencyList.AddEdge(vertex1,vertex2);
            IncidencyMatrix.AddEdge(vertex1,vertex2);
        }

        public void RemoveEdge(int vertex1, int vertex2)
        {
            AdjacencyMatrix.RemoveEdge(vertex1,vertex2);
            AdjacencyList.RemoveEdge(vertex1,vertex2);
            IncidencyMatrix.RemoveEdge(vertex1,vertex2);
        }

        public void MergeVertices(int vertex1, int vertex2)
        {
            AdjacencyMatrix = new OrientedAdjacencyMatrix((AdjacencyMatrix)AdjacencyMatrix.MergeNodes(vertex1, vertex2));
            AdjacencyList = new OrientedAdjacencyList((AdjacencyList)AdjacencyList.MergeNodes(vertex1, vertex2));
            IncidencyMatrix = (IncidencyMatrix)IncidencyMatrix.MergeNodes(vertex1, vertex2);
        }

        public async Task<Search<AdjacencyList>> SearchAsync(int startNode, SearchType searchType)
        {
            var search = new Search<AdjacencyList>(AdjacencyList);
            if (searchType == SearchType.BFS)
            {
                await search.BFSAsync(startNode);
            }
            else
            {
                await search.DFSAsync(startNode);
            }
            return search;
        }
        
    }
}
