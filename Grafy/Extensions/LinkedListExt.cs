﻿using System;
using System.Collections.Generic;


namespace Grafy.Extensions
{
    public static class LinkedListExt
    {
        public static LinkedListNode<int> AddUniqueSortedNode(this LinkedList<int> list, int value)
        {
            if (list.Count == 0)
            {
                list.AddFirst(value);
                return list.First;
            }

            var node = list.First;

            for (var i = 0; i < list.Count; i++,node=node?.Next)
            {
                if (node?.Value == value)
                {
                    return node;
                }


                if (node.Value <= value) continue;

                list.AddBefore(node, value);

                return node.Previous;
            }
            list.AddLast(value);
            return list.Last;
        }

        
        public static LinkedListNode<int> AddSortedNode(this LinkedList<int> list, int value)
        {
            if (list.Count == 0)
            {
                list.AddFirst(value);
                return list.First;
            }

            var node = list.First;

            for (var i = 0; i < list.Count; i++, node = node.Next)
            {
                if (node.Value <= value && node.Value != value) continue;

                list.AddBefore(node, value);

                return node.Previous;
            }

            return null;
        }
        public static int ValueAt(this LinkedList<int> list, int index)
        {
            if (list.Count == 0)
            {
                throw new NullReferenceException();
            }

            if (index >= list.Count)
            {
                throw new IndexOutOfRangeException();
            }

            var node = list.First;

            for (var i = 0; i < index; i++, node = node.Next)
            {
            }

            return node.Value;

        }
    }
}
