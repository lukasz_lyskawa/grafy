﻿using System;
using Grafy.Interface;

namespace Grafy.Representations
{
    public class WeightedAdjacencyMatrix : AdjacencyMatrix, IWeighted
    {
        public WeightedAdjacencyMatrix()
        {

        }

        public WeightedAdjacencyMatrix(int nodeAmount) : base(nodeAmount)
        {

        }

        public WeightedAdjacencyMatrix(AdjacencyMatrix baseobject)
        {
            NodeCounter = baseobject.NodeCounter;
            EdgeCounter = baseobject.EdgeCounter;
            Matrix = baseobject.Matrix.Copy();
        }

        [Obsolete("Use AddEdge(x,y,weight) instead.", true)]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override void AddEdge(int vertex1, int vertex2)
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
        }


        public void AddEdge(int vertex1, int vertex2, int weight)
        {
            Matrix[vertex1, vertex2] += weight;
            Matrix[vertex2, vertex1] += weight;
            EdgeCounter++;
        }

        public override IGraphRepresentation MergeNodes(int node1, int node2)
        {
            var copy = (WeightedAdjacencyMatrix)DeepCopy();


            for (int i = 0; i < NodeCounter; i++)
            {
                copy.Matrix[i, node1] += Matrix[i, node2];

                copy.Matrix[node1, i] += Matrix[node2, i];
            }
            copy.RemoveNode(node2);

            for (int i = 0; i < copy.NodeCounter; i++)
            {
                copy.Matrix[i, i] = 0;
            }

            return copy;
        }
        public override ICopyable DeepCopy()
        {
            return new WeightedAdjacencyMatrix
            {
                NodeCounter = NodeCounter,
                EdgeCounter = EdgeCounter,
                Matrix = Matrix.Copy()
            };
        }
    }
}
