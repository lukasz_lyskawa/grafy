﻿using Grafy.Interface;

namespace Grafy.Representations
{
    public class OrientedAdjacencyMatrix : AdjacencyMatrix, IOriented
    {
        public OrientedAdjacencyMatrix(int nodeAmount) : base(nodeAmount)
        {

        }

        public OrientedAdjacencyMatrix()
        {
            
        }
        public OrientedAdjacencyMatrix(AdjacencyMatrix am)
        {
            NodeCounter = am.NodeCounter;
            EdgeCounter = am.EdgeCounter;
            Matrix = am.Matrix.Copy();
        }

        public OrientedAdjacencyMatrix(OrientedAdjacencyMatrix am)
        {
            NodeCounter = am.NodeCounter;
            EdgeCounter = am.EdgeCounter;
            Matrix = am.Matrix.Copy();
        }

        public override void AddEdge(int vertex1, int vertex2)
        {
            Matrix[vertex1, vertex2] = 1;
            EdgeCounter++;
        }

        public override void RemoveEdge(int vertex1, int vertex2)
        {
            if (Matrix[vertex1, vertex2] <= 0) return;
            Matrix[vertex1, vertex2] = 0;
            EdgeCounter--;
        }



        public override IGraphRepresentation Transpose()
        {

            var result = new OrientedAdjacencyMatrix(NodeCounter);

            for (int x = 0; x < NodeCounter; x++)
            {
                for (int y = 0; y < NodeCounter; y++)
                {

                    if (Matrix[x, y] != 0)
                    {
                        result.AddEdge(y, x);
                    }
                }
            }

            return result;
        }

        public override ICopyable DeepCopy()
        {
            return new OrientedAdjacencyMatrix
            {
                NodeCounter = NodeCounter,
                EdgeCounter = EdgeCounter,
                Matrix = Matrix.Copy()
            };
        }
    }
}
