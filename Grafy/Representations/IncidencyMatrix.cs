﻿using System.Diagnostics;
using Grafy.Interface;

namespace Grafy.Representations
{
    public class IncidencyMatrix : IGraphRepresentation, ICopyable
    {
        public int this[int x, int y]
        {
            get { return _matrix[x, y]; }
            set { _matrix[x, y] = value; }
        }

        private Matrix<int> _matrix;

        public int EdgeCounter { get; protected set; }
        public int NodeCounter { get; protected set; }
        


        public IncidencyMatrix()
        {
            _matrix = new Matrix<int>();
        }

        public IncidencyMatrix(int nodeAmount)
        {
            _matrix = new Matrix<int>();
            AddNode(nodeAmount);
        }



        public void AddNode(int amount=1)
        {
            for (int i = 0; i < amount; i++)
            {
                _matrix.AddRow();
                NodeCounter++;
                for (int j = 0; j < EdgeCounter; j++)
                {
                    _matrix[NodeCounter-1].Add(0);
                }
            }
        }

        public void RemoveNode(int nodeIndex)
        {
            var edges = EdgeCounter;
            for (int i = edges-1; i >= 0; i--)
            {
                if (_matrix[nodeIndex, i] > 0)
                {
                    RemoveEdge(i);
                }
            }

            _matrix.RemoveRow(nodeIndex);
            NodeCounter--;
        }

        public virtual IGraphRepresentation MergeNodes(int node1, int node2)
        {
            var graph = (IncidencyMatrix)DeepCopy();
            int delete=-1;
            for (int i = 0; i < EdgeCounter; i++)
            {
                var g1 = graph[node2,i];
                var g2 = _matrix[node1,i];
                Debug.WriteLine(graph.ToString());
                if (graph[node2,i] > 0 && _matrix[node1,i] >0)
                {
                    delete = i;
                    continue;
                }
                if (graph[node2,i] > 0)
                {
                    graph[node1,i] += 1;
                }
            }
            if (delete > -1)
            {
                graph.RemoveEdge(delete);
            }
            graph.RemoveNode(node2);

            return graph;


        }


        public virtual void AddEdge(int vertex1, int vertex2)
        {
            _matrix.AddCol();
            _matrix[vertex1,_matrix.Size().Item2-1] += 1;
            _matrix[vertex2,_matrix.Size().Item2-1] += 1;
            EdgeCounter++;
        }

        public virtual void RemoveEdge(int edgeId)
        {
            _matrix.RemoveCol(edgeId);
            EdgeCounter--;
        }

        public virtual void RemoveEdge(int vertex1,int vertex2)
        {
            for (int i = 0; i < _matrix.Size().Item2; i++)
            {
                if (_matrix[vertex1,i] == 1 && _matrix[vertex2,i] == 1)
                {
                    _matrix.RemoveCol(i);
                }
            }
            EdgeCounter--;
        }

        public IGraphRepresentation Transpose()
        {
            return this;
        }


        public override string ToString()
        {
            if (NodeCounter == 0)
            {
                return "Empty graph";
            }
            string result = "  e│  ";
            for (int i = 0; i < _matrix.Size().Item2; i++)
            {
                result += $"{i} ";
            }
            result += "\nv  │";
            result += "\n───┼──";
            for (int i = 0; i < _matrix.Size().Item2; i++)
            {
                result += "──";
            }
            result += "\n";

            for (int i = 0; i < _matrix.Size().Item1; i++)
            {
                result += $"{i}  │  ";
                for (int j = 0; j < _matrix.Size().Item2; j++)
                {
                    result += $"{_matrix[i, j]} ";
                }
                result += "\n";
            }
           
            return result;
        }

        public ICopyable DeepCopy()
        {
            return new IncidencyMatrix
            {
                NodeCounter = NodeCounter,
                EdgeCounter = EdgeCounter,
                _matrix = _matrix.Copy()
            };
        }
    }
}
