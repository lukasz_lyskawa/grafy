﻿using System;
using Grafy.Interface;

namespace Grafy.Representations
{
    public class WeightedOrientedAdjacencyMatrix : OrientedAdjacencyMatrix, IWeighted
    {
        public WeightedOrientedAdjacencyMatrix(int nodeAmount) : base(nodeAmount)
        { 
        }

        [Obsolete("Use AddEdge(x,y,weight) instead.", true)]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override void AddEdge(int vertex1, int vertex2)
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
        {
        }

        public WeightedOrientedAdjacencyMatrix(AdjacencyMatrix baseobject) : base()
        {
            NodeCounter = baseobject.NodeCounter;
            EdgeCounter = baseobject.EdgeCounter;
            Matrix = baseobject.Matrix.Copy();
        }

        public WeightedOrientedAdjacencyMatrix()
        {
        }

        public void AddEdge(int vertex1, int vertex2, int weight)
        {
            Matrix[vertex1, vertex2] = weight;
            EdgeCounter++;
        }

        public override IGraphRepresentation Transpose()
        {
            var result = new WeightedOrientedAdjacencyMatrix(NodeCounter);

            for (int x = 0; x < NodeCounter; x++)
            {
                for (int y = 0; y < NodeCounter; y++)
                {

                    if (Matrix[x, y] != 0)
                    {
                        result.AddEdge(y, x,Matrix[x,y]);
                    }
                }
            }

            return result;
        }

        public override IGraphRepresentation MergeNodes(int node1, int node2)
        {
            var copy = (WeightedOrientedAdjacencyMatrix)DeepCopy();


            for (int i = 0; i < NodeCounter; i++)
            {
                copy.Matrix[i, node1] += Matrix[i, node2];

                copy.Matrix[node1, i] += Matrix[node2, i];
            }
            copy.RemoveNode(node2);

            for (int i = 0; i < copy.NodeCounter; i++)
            {
                copy.Matrix[i, i] = 0;
            }

            return copy;
        }

        public override ICopyable DeepCopy()
        {
            return new WeightedOrientedAdjacencyMatrix
            {
                NodeCounter = NodeCounter,
                EdgeCounter = EdgeCounter,
                Matrix = Matrix.Copy()
            };
        }
    }
}
