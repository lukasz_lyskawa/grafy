﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Grafy.Extensions;
using Grafy.Interface;

namespace Grafy.Representations
{
    [DataContract]
    public class AdjacencyList : IGraphRepresentation , ICopyable
    {

        public int this[int x, int y] => List[x].ElementAt(y);

        public List<LinkedList<int>> List { get; set; }
        public int NodeCounter { get; protected set; }
        public int EdgeCounter { get; protected set; }



        public AdjacencyList()
        {
            List = new List<LinkedList<int>>();
        }

        public AdjacencyList(int nodeAmount)
        {
            List = new List<LinkedList<int>>();
            AddNode(nodeAmount);
        }

        protected AdjacencyList(List<LinkedList<int>> list, int nodeCounter, int edgeCounter )
        {
            List = list;
            NodeCounter = nodeCounter;
            EdgeCounter = edgeCounter;
        }




        public void AddNode(int amount = 1)
        {
            for (int i = 0; i < amount; i++)
            {
                List.Add(new LinkedList<int>());
                NodeCounter++;
            }
        }

        public void RemoveNode(int nodeIndex) //todo untested
        {
            foreach (var node in List.Where(node => node.Contains(nodeIndex)))
            {
                node.Remove(nodeIndex);
            }


            for (int i = 0; i < List.Count; i++)
            {
                var node = List[i].First;
                for (int j = 0; j < List[i].Count; j++, node = node.Next)
                {
                    if (node != null && node.Value > nodeIndex)
                    {
                        node.Value--;
                    }

                }
            }
            
            List.RemoveAt(nodeIndex);
            NodeCounter--;
        }

        public virtual IGraphRepresentation MergeNodes(int node1, int node2)
        {
            
            var copy = (AdjacencyList)DeepCopy();

            copy.RemoveEdge(node1,node2);

            foreach (var adjacentNode in copy.List[node2])
            {
                copy.List[node1].AddUniqueSortedNode(adjacentNode);
                copy.List[node1].Remove(node2);
            }

            copy.RemoveNode(node2);

            return copy;
        }


        public virtual void AddEdge(int node1, int node2)
        {
            List[node1].AddUniqueSortedNode(node2);
            List[node2].AddUniqueSortedNode(node1);

            EdgeCounter++;
        }

        public virtual void RemoveEdge(int node1, int node2)
        {
            List[node1].Remove(node2);
            List[node2].Remove(node1);

            EdgeCounter--;
        }

        public virtual IGraphRepresentation Transpose()
        {
            return this;
        }

        public virtual ICopyable DeepCopy()
        {
            var result = new AdjacencyList(NodeCounter);

            for (int i = 0; i < NodeCounter; i++)
            {
                for (int j = 0; j < List[i].Count; j++)
                {
                    result.AddEdge(i,List[i].ValueAt(j));
                }
            }

            return result;
        }


        public override string ToString()
        {
            if (NodeCounter == 0)
            {
                return "Empty graph";
            }
            var result = "";
            for (int i = 0; i < List.Count; i++)
            {
                result += $"v{i}:\t";
                result = List[i].Aggregate(result, (current, vertexList) => current + (vertexList + ", "));
                result += "\n";
            }
            return result;
        }

    }
}
