﻿using System.Collections.Generic;
using System.Linq;
using Grafy.Extensions;
using Grafy.Interface;

namespace Grafy.Representations
{
    public sealed class OrientedAdjacencyList : AdjacencyList, IOriented
    {
        public OrientedAdjacencyList()
        {
            
        }
        public OrientedAdjacencyList(int vertexAmount) : base(vertexAmount)
        {

        }

        public OrientedAdjacencyList(List<LinkedList<int>> list, int nodeCounter, int edgeCounter ): base(list,nodeCounter,edgeCounter)
        {
            
        }

        public OrientedAdjacencyList(AdjacencyList al)
        {
            List = al.List.ToList();
            NodeCounter = al.NodeCounter;
            EdgeCounter = al.EdgeCounter;
        }

        public override IGraphRepresentation Transpose()
        {
            var result = new OrientedAdjacencyList(NodeCounter);

            for (int edgeFrom = 0; edgeFrom < List.Count; edgeFrom++)
            {
                foreach (int edgeTo in List[edgeFrom])
                {
                    result.AddEdge(edgeTo,edgeFrom);
                }
            }

            return result;
        }

        public override ICopyable DeepCopy()
        {
            var result = new OrientedAdjacencyList(NodeCounter);

            for (int i = 0; i < NodeCounter; i++)
            {
                for (int j = 0; j < List[i].Count; j++)
                {
                    result.AddEdge(i, List[i].ValueAt(j));
                }
            }

            return result;
        }

        public override IGraphRepresentation MergeNodes(int node1, int node2)
        {

            var copy = (OrientedAdjacencyList)DeepCopy();

            copy.RemoveEdge(node1, node2);

            foreach (var adjacentNode in copy.List[node2])
            {
                if (node1 != adjacentNode)
                {
                    copy.List[node1].AddUniqueSortedNode(adjacentNode);
                    copy.List[node1].Remove(node2);
                }
                
            }

            copy.RemoveNode(node2);

            return copy;
        }

        public override void AddEdge(int node1, int node2)
        {
            List[node1].AddUniqueSortedNode(node2);
            EdgeCounter++;
        }

        public override void RemoveEdge(int node1, int node2)
        {
            List[node1].Remove(node2);
            EdgeCounter--;
        }
    }
}
