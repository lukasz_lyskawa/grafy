﻿using Grafy.Interface;

namespace Grafy.Representations
{
    public class AdjacencyMatrix : IGraphRepresentation, ICopyable
    {
        public Matrix<int> Matrix { get; set; }

        public int this[int x, int y]
        {
            get { return Matrix[x, y]; }
            set { Matrix[x, y] = value; }
        }

        public int EdgeCounter { get; protected set; }
        public int NodeCounter { get; protected set; }



        public AdjacencyMatrix()
        {
            Matrix = new Matrix<int>();
        }

        public AdjacencyMatrix(int nodeAmount)
        {
            Matrix = new Matrix<int>();
            AddNode(nodeAmount);
        }


        public void AddNode(int amount = 1)
        {
            for (int i = 0; i < amount; i++)
            {
                Matrix.AddRow();
                Matrix.AddCol();
                NodeCounter++;
            }
        }

        public void RemoveNode(int nodeIndex)
        {
            Matrix.RemoveRow(nodeIndex);
            Matrix.RemoveCol(nodeIndex);
            NodeCounter--;
        }
        public virtual void AddEdge(int vertex1, int vertex2)
        {
            if (Matrix[vertex1, vertex2] >= 1) return;
            Matrix[vertex1, vertex2] = 1;
            Matrix[vertex2, vertex1] = 1;
            EdgeCounter++;
        }

        public virtual void RemoveEdge(int vertex1, int vertex2)
        {
            if (Matrix[vertex1, vertex2] <= 0) return;
            Matrix[vertex1, vertex2] = 0;
            Matrix[vertex2, vertex1] = 0;
            EdgeCounter--;
        }

        public virtual IGraphRepresentation MergeNodes(int node1, int node2)
        {
            if (NodeCounter == 0) return this;

            var copy = (AdjacencyMatrix)DeepCopy();

            /*
            if row[node1]==0 val from row[node2] to row[node1]
            if col[node1]==0 val from col[node2] to col[node1]
            clear diagonal

            */
            for (int i = 0; i < NodeCounter; i++)
            {
                if (Matrix[i, node1] == 0)
                {
                    copy.Matrix[i, node1] += Matrix[i, node2];
                }
                if (Matrix[node1, i] == 0)
                {
                    copy.Matrix[node1, i] += Matrix[node2, i];
                }

            }
            copy.RemoveNode(node2);

            for (int i = 0; i < copy.NodeCounter; i++)
            {
                copy.Matrix[i, i] = 0;
            }

            return copy;
        }

        public virtual IGraphRepresentation Transpose()
        {
            return this;
        }

        public virtual ICopyable DeepCopy()
        {
            return new AdjacencyMatrix
            {
                NodeCounter = NodeCounter,
                EdgeCounter = EdgeCounter,
                Matrix = Matrix.Copy()
            };
        }

        public override string ToString()
        {
            if (NodeCounter == 0)
            {
                return "Empty graph";
            }
            string result = "v│  ";

            for (int i = 0; i < Matrix.Size().Item1; i++)
            {
                result += $"{i} ";
            }
            result += "\n" +
                      "─┼──";
            for (int i = 0; i < Matrix.Size().Item1; i++)
            {
                result += "──";
            }
            result += "\n";
            for (int i = 0; i < Matrix.Size().Item1; i++)
            {
                result += $"{i}│  ";
                for (int j = 0; j < Matrix.Size().Item2; j++)
                {
                    result += $"{Matrix[i, j]} ";
                }
                result += "\n";
            }

            return result;
        }
    }
}
