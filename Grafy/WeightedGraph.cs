﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grafy.Functions;
using Grafy.Interface;
using Grafy.Representations;

namespace Grafy
{
    public class WeightedGraph : IWeightedGraph, IGraph
    {
        public WeightedAdjacencyMatrix AdjacencyMatrix = new WeightedAdjacencyMatrix();
        public AdjacencyList AdjacencyList = new AdjacencyList();
        public IncidencyMatrix IncidencyMatrix = new IncidencyMatrix();

        private bool _graphChanged = true;
        private Floyd _floyd;

        public async Task<bool> IsConnected()
        {
            var componentOperation = new Component<WeightedAdjacencyMatrix>(AdjacencyMatrix);
            await componentOperation.ComputeAsync();
            return componentOperation.IsConnected != null && componentOperation.IsConnected.Value;
        }

        public async Task<int> Components()
        {
            var componentOperation = new Component<WeightedAdjacencyMatrix>(AdjacencyMatrix);
            return await componentOperation.ComputeAsync();
        }

        public void AddVertex(int count = 1)
        {
            AdjacencyMatrix.AddNode(count);
            AdjacencyList.AddNode(count);
            IncidencyMatrix.AddNode(count);
            _graphChanged = true;
        }

        public void RemoveVertex(int vertex)
        {
            AdjacencyMatrix.RemoveNode(vertex);
            AdjacencyList.RemoveNode(vertex);
            IncidencyMatrix.RemoveNode(vertex);
            _graphChanged = true;
        }

        [Obsolete("Use AddEdge(x,y,weight) instead.", true)]
        public void AddEdge(int vertex1, int vertex2)
        {
        }

        public void AddEdge(int vertex1, int vertex2, int weight)
        {
            AdjacencyMatrix.AddEdge(vertex1, vertex2, weight);
            AdjacencyList.AddEdge(vertex1, vertex2);
            IncidencyMatrix.AddEdge(vertex1, vertex2);
            _graphChanged = true;
        }

        public void RemoveEdge(int vertex1, int vertex2)
        {
            AdjacencyMatrix.RemoveEdge(vertex1, vertex2);
            AdjacencyList.RemoveEdge(vertex1, vertex2);
            IncidencyMatrix.RemoveEdge(vertex1, vertex2);
            _graphChanged = true;
        }

        public void MergeVertices(int vertex1, int vertex2)
        {
            AdjacencyMatrix.MergeNodes(vertex1, vertex2);
            AdjacencyList.MergeNodes(vertex1, vertex2);
            IncidencyMatrix.MergeNodes(vertex1, vertex2);
            _graphChanged = true;
        }

        public async Task<Search<AdjacencyList>> SearchAsync(int startNode, SearchType searchType)
        {
            var search = new Search<AdjacencyList>(AdjacencyList);
            if (searchType == SearchType.BFS)
            {
                await search.BFSAsync(startNode);
            }
            else
            {
                await search.DFSAsync(startNode);
            }
            return search;
        }

        public int Diameter
        {
            get
            {
                if (_graphChanged)
                {
                    _floyd = new Floyd(AdjacencyMatrix);
                    _graphChanged = false;
                }
                    
                return _floyd.Diameter;
            }
        }

        public int Radius
        {
            get
            {
                if (_graphChanged)
                {
                    _floyd = new Floyd(AdjacencyMatrix);
                    _graphChanged = false;
                }
                return _floyd.Radius;
            }
        }

        public List<int> CentralVertices
        {
            get
            {
                if (_graphChanged)
                {
                    _floyd = new Floyd(AdjacencyMatrix);
                    _graphChanged = false;
                }
                return _floyd.CentralVertices;
            }
        }

        public int DistanceBetweenVertices(int vertex1, int vertex2)
        {
            if (_graphChanged)
            {
                _floyd = new Floyd(AdjacencyMatrix);
                _graphChanged = false;
            }
                
            return _floyd.ClosestDistanceBetween(vertex1, vertex2);
        }
    }
}
