﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Grafy
{
    public class Matrix<T>
    {
        private List<List<T>> _matrix;

        public T this[int x, int y]
        {
            get { return _matrix[x][y]; }
            set { _matrix[x][y] = value; }
        }
        public List<T> this[int y]
        {
            get { return _matrix[y]; }
            set { _matrix[y] = value; }
        }

        public Matrix()
        {
            _matrix = new List<List<T>>();
        }

        public void AddRow()
        {
            var newRow = new List<T>();

            _matrix.Add(newRow);
        }

        public void AddRow(IList<T> row)
        {
            var newRow = new List<T>(row);

            _matrix.Add(newRow);
        }


        public void RemoveRow(int position)
        {
            _matrix.RemoveAt(position);
        }



        public void AddCol()
        {
            for (int i = 0; i < _matrix.Count; i++)
            {
                if (i == 0)
                {
                    _matrix[0].Add(default(T));
                }
                else
                {
                    for (int j = _matrix[i].Count; j < _matrix[0].Count; j++)
                    {
                        _matrix[i].Add(default(T));
                    }
                }
                
            }
        }

        public void AddCol(IList<T> col)
        {
            for (int i = 0; i < _matrix.Count; i++)
            {
                _matrix[i].Add(col[i]);
            }
        }


        public void RemoveCol(int position)
        {
            foreach (List<T> row in _matrix)
            {
                row.RemoveAt(position);
            }
        }


        public Tuple<int,int> Size()
        {
            return new Tuple<int, int>(_matrix.Count,_matrix[0].Count);
        }

        public Matrix<T> Copy()
        {
            return new Matrix<T> {_matrix = _matrix.Select(x => x.ToList()).ToList()};
        }

    }
}
