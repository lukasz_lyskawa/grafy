﻿using System.Windows;
using Caliburn.Micro;

namespace WpfClient.Models
{
    public class Tool : PropertyChangedBase
    {
        private double _x;

        private double _y;

        private Visibility _toolVisibility;

        private double _offsetX;

        private double _offsetY;

        public double X
        {
            get { return _x; }
            set
            {
                _x = value;
                NotifyOfPropertyChange();
            }
        }

        public double Y
        {
            get { return _y; }
            set
            {
                _y = value;
                NotifyOfPropertyChange();
            }
        }

        public Visibility ToolVisibility
        {
            get { return _toolVisibility; }
            set
            {
                _toolVisibility = value;
                NotifyOfPropertyChange();
            }
        }

        public double OffsetX
        {
            get { return _offsetX; }
            set { _offsetX = value; NotifyOfPropertyChange(); }
        }

        public double OffsetY
        {
            get { return _offsetY; }
            set { _offsetY = value; NotifyOfPropertyChange(); }
        }
    }
}
