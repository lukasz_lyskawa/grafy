﻿using System.Runtime.Serialization;
using Caliburn.Micro;

namespace WpfClient.Models
{
    [DataContract]
    public class Vertex : PropertyChangedBase
    {

        private bool _isDragging;

        public bool IsDragging
        {
            get
            {
                return _isDragging;
            }
            set
            {
                _isDragging = value;
                NotifyOfPropertyChange();
            }
        }
        [DataMember]
        private double _x;

        public double X
        {
            get
            {
                return _x;
            }
            set
            {
                _x = value;
                NotifyOfPropertyChange();
            }
        }
        [DataMember]
        private double _y;

        public double Y
        {
            get
            {
                return _y;
            }
            set
            {
                _y = value;
                NotifyOfPropertyChange();
            }
        }
        [DataMember]
        private int _number;

        public int Number
        {
            get
            {
                return _number;
            }
            set
            {
                _number = value;
                NotifyOfPropertyChange();
            }
        }
    }
}
