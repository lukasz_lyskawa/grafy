﻿using System;

namespace WpfClient.Models
{
    [Flags]
    public enum GraphType
    {
        Normal=0,
        Oriented=2,
        Weighted=4,
    }
}
