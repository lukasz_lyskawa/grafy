﻿using System;
using System.Runtime.Serialization;
using Caliburn.Micro;

namespace WpfClient.Models
{
    [DataContract]
    public class Edge : PropertyChangedBase
    {

        [DataMember]
        private Vertex _fromVertex;

        public Vertex FromVertex
        {
            get
            {
                return _fromVertex;
            }
            set
            {

                _fromVertex = value;
                NotifyOfPropertyChange();
            }
        }
        [DataMember]
        private Vertex _toVertex;

        public Vertex ToVertex
        {
            get
            {
                return _toVertex;
            }
            set
            {
                _toVertex = value;
                NotifyOfPropertyChange();
            }
        }
        [DataMember]
        private int _weight;

        public int Weight
        {
            get
            {
                return _weight;
            }
            set
            {
                _weight = value;
                NotifyOfPropertyChange();
            }
        }
    }
}
