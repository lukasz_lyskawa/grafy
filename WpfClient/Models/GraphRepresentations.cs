﻿namespace WpfClient.Models
{
    public enum GraphRepresentations
    {
        None,
        AdjacencyList,
        AdjacencyMatrix,
        IncidencyMatrix
    }

}
