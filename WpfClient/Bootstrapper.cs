﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Input;
using Caliburn.Micro;
using WpfClient.ViewModels;

namespace WpfClient
{
    public class Bootstrapper : BootstrapperBase
    {
        public Bootstrapper()
        {
            Initialize();
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.CreateSpecificCulture("pl-PL");
        }

        private readonly SimpleContainer _container = new SimpleContainer();


        protected override object GetInstance(Type service, string key)
        {
            return _container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        protected override void Configure()
        {
            base.Configure();

            _container.RegisterSingleton(typeof (IWindowManager), "wm", typeof (WindowManager));
            _container.RegisterSingleton(typeof (IEventAggregator), "ev", typeof (EventAggregator));
            _container.RegisterSingleton(typeof (ShellViewModel), "shell", typeof (ShellViewModel));

            MessageBinder.SpecialValues.Add("$keypressed", context =>
            {
                var keyArgs = context.EventArgs as KeyEventArgs;

                return keyArgs?.Key;
            });
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            IoC.Get<IWindowManager>("wm").ShowWindow(IoC.Get<ShellViewModel>("shell"));
        }
    }
}
