﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace WpfClient.ViewModels
{
    public class DialogDataInputViewModel<T> : DialogViewModel
    {
        private T _input;
        private List<string> _options;

        public DialogDataInputViewModel(string message) : base(message)
        {
        }

        public T Input
        {
            get { return _input; }
            set { _input = value; NotifyOfPropertyChange(); }
        }

        public List<string> Options
        {
            get { return _options; }
            set { _options = value; NotifyOfPropertyChange(); }
        }

        public void Confirm()
        {
            TryClose(true);
        }

        public void Cancel()
        {
            TryClose(false);
        }
    }
}
