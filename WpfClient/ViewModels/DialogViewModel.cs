﻿using Caliburn.Micro;

namespace WpfClient.ViewModels
{
    public class DialogViewModel : Screen
    {
        private string _message;
        public DialogViewModel(string message)
        {
           Message=message;
        }

        public string Message
        {
            get { return _message; }
            set { _message = value;NotifyOfPropertyChange(); }
        }
    }
}
