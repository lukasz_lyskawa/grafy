﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Caliburn.Micro;
using Grafy;
using Grafy.Functions;
using Grafy.Representations;
using WpfClient.Models;

namespace WpfClient.ViewModels
{
    [DataContract]
    public class OrientedCanvasViewModel : Screen
    {
        private GraphRepresentations _graphRepresentationName;

        private string _graphRepresentationString;

        private Cursor _cursor = Cursors.Arrow;
        [DataMember]
        private BindableCollection<Edge> _edges;
        [DataMember]
        private BindableCollection<Vertex> _vertices;

        private Vertex _connectWith;
        private Vertex _mergeWith;


        private IEventAggregator _ev;
        private IWindowManager _wm;


        private Visibility _displayVisibility = Visibility.Collapsed;
        public Visibility DisplayVisibility
        {
            get { return _displayVisibility; }
            set { _displayVisibility = value; NotifyOfPropertyChange(); }
        }

        public Tool CanvasTools { get; set; }

        public Tool VertexTools { get; set; }

        public Tool EdgeTools { get; set; }

        public object LastSelected { get; set; }


        public BindableCollection<Edge> Edges
        {
            get { return _edges; }
            set { _edges = value; NotifyOfPropertyChange(); }
        }

        public BindableCollection<Vertex> Vertices
        {
            get { return _vertices; }
            set { _vertices = value; NotifyOfPropertyChange(); }
        }

        public string GraphRepresentationName
        {
            get
            {
                return Enum.GetName(typeof(GraphRepresentations), _graphRepresentationName);
            }
            set { _graphRepresentationName = (GraphRepresentations)Enum.Parse(typeof(GraphRepresentations), value); NotifyOfPropertyChange(); }
        }

        public string GraphRepresentationString
        {
            get { return _graphRepresentationString; }
            set { _graphRepresentationString = value; NotifyOfPropertyChange(); }
        }

        public Cursor Cursor
        {
            get
            {
                return _cursor;
            }
            set
            {
                _cursor = value;
                NotifyOfPropertyChange();
            }
        }

        public BindableCollection<ViewModels.OrientedCanvasViewModel> History;

        public OrientedGraph GraphCore;



        public OrientedCanvasViewModel()
        {
            DisplayName = "Oriented";
            Vertices = new BindableCollection<Vertex>();
            Edges = new BindableCollection<Edge>();
            GraphCore = new OrientedGraph();
            CanvasTools = new Tool { ToolVisibility = Visibility.Collapsed, OffsetX = 10, OffsetY = -40 };
            VertexTools = new Tool { ToolVisibility = Visibility.Collapsed, OffsetX = 10, OffsetY = -40 };
            EdgeTools = new Tool { ToolVisibility = Visibility.Collapsed, OffsetX = 10, OffsetY = -40 };
            History = new BindableCollection<OrientedCanvasViewModel>();

            GraphRepresentationName = Enum.GetName(typeof(GraphRepresentations), GraphRepresentations.None);

            _ev = IoC.Get<IEventAggregator>("ev");
            _wm = IoC.Get<IWindowManager>("wm");
        }

        public OrientedCanvasViewModel(ViewModels.OrientedCanvasViewModel activeItem, Search<AdjacencyList> path)
        {
            GraphRepresentationName = Enum.GetName(typeof(GraphRepresentations), GraphRepresentations.None);
            CanvasTools = new Tool { ToolVisibility = Visibility.Collapsed };
            VertexTools = new Tool { ToolVisibility = Visibility.Collapsed };
            EdgeTools = new Tool { ToolVisibility = Visibility.Collapsed };

            Vertices = new BindableCollection<Vertex>(activeItem.Vertices.ToList());
            Edges = new BindableCollection<Edge>(activeItem.Edges.ToList());
            for (int i = 0; i < Vertices.Count; i++)
            {
                if (!path.Path.Contains(Vertices[i].Number))
                {
                    Vertices.RemoveAt(i);
                }
            }
            for (int i = 0; i < Edges.Count; i++)
            {
                if (!path.Tree.List[Edges[i].FromVertex.Number].Contains(Edges[i].ToVertex.Number) ||
                    !path.Tree.List[Edges[i].ToVertex.Number].Contains(Edges[i].FromVertex.Number))
                {
                    Edges.RemoveAt(i);
                }
            }

        }

        public OrientedCanvasViewModel(OrientedCanvasViewModel deserializedObj)
        {
            Vertices = new BindableCollection<Vertex>();
            foreach (var vertex in deserializedObj.Vertices)
            {
                Vertices.Add(new Vertex
                {
                    X = vertex.X,
                    Y = vertex.Y,
                    Number = vertex.Number
                });
            }

            Edges = new BindableCollection<Edge>();
            foreach (var edge in deserializedObj.Edges)
            {
                Edges.Add(new Edge
                {
                    FromVertex = Vertices[edge.FromVertex.Number],
                    ToVertex = Vertices[edge.ToVertex.Number],
                    Weight = edge.Weight
                });
            }
            GraphCore = deserializedObj.GraphCore;
            CanvasTools = new Tool { ToolVisibility = Visibility.Collapsed, OffsetX = 10, OffsetY = -40 };
            VertexTools = new Tool { ToolVisibility = Visibility.Collapsed, OffsetX = 10, OffsetY = -40 };
            EdgeTools = new Tool { ToolVisibility = Visibility.Collapsed, OffsetX = 10, OffsetY = -40 };
            History = new BindableCollection<OrientedCanvasViewModel>();

            GraphRepresentationName = Enum.GetName(typeof(GraphRepresentations), GraphRepresentations.None);

            _ev = IoC.Get<IEventAggregator>("ev");
            _wm = IoC.Get<IWindowManager>("wm");
        }


        public void HistoryBack()
        {
            Edges = History[History.Count - 1].Edges;
            Vertices = History[History.Count - 1].Vertices;
            GraphCore = History[History.Count - 1].GraphCore;
            History.Remove(History[History.Count - 1]);

            GraphChanged();
        }

        public void AddToHistory()
        {

            History.Add(new ViewModels.OrientedCanvasViewModel
            {
                GraphCore = new OrientedGraph()
                {
                    AdjacencyList = new OrientedAdjacencyList(GraphCore.AdjacencyList.List,GraphCore.AdjacencyList.NodeCounter,GraphCore.AdjacencyList.EdgeCounter),
                    AdjacencyMatrix = new OrientedAdjacencyMatrix(GraphCore.AdjacencyMatrix),
                    IncidencyMatrix = (IncidencyMatrix)GraphCore.IncidencyMatrix.DeepCopy()
                },
                Edges = new BindableCollection<Edge>(Edges.ToList()),
                Vertices = new BindableCollection<Vertex>(Vertices.ToList())
            });
        }
        public virtual void VertexMouseDown(Vertex sender, MouseButtonEventArgs args)
        {
            if (_mergeWith != null)
            {
                AddToHistory();

                Merge(sender);

                LastSelected = null;
                _mergeWith = null;
                Cursor = Cursors.Arrow;

                UpdateVertexNumeration();

                GraphChanged();

                return;
            }
            if (_connectWith != null)
            {
                AddToHistory();

                AddEdge(sender);

                _connectWith = null;
                LastSelected = null;
                Cursor = Cursors.Arrow;

                GraphChanged();
                return;
            }
            if (args.LeftButton == MouseButtonState.Pressed)
            {
                sender.IsDragging = true;
            }
            else
            {
                LastSelected = sender;
            }
            GraphChanged();
        }
        public void PreAddEdge()
        {
            _connectWith = (Vertex)LastSelected;
            Cursor = Cursors.Cross;
            VertexTools.ToolVisibility = Visibility.Collapsed;
            GraphChanged();
        }
        public virtual void AddEdge(Vertex sender)
        {
            Edges.Add(new Edge { FromVertex = _connectWith, ToVertex = sender });
            GraphCore.AddEdge(_connectWith.Number, sender.Number);
        }

        public void PreMerge()
        {
            _mergeWith = (Vertex)LastSelected;
            Cursor = Cursors.Cross;
            VertexTools.ToolVisibility = Visibility.Collapsed;
        }
        public virtual void Merge(Vertex sender)
        {
            var edgesToDel = new List<int>();
            for (int i = 0; i < Edges.Count; i++)
            {
                if (Edges[i].FromVertex == _mergeWith && Edges[i].ToVertex != sender)
                {
                    Edges[i].FromVertex = sender;
                    continue;
                }
                if ((Edges[i].FromVertex == _mergeWith && Edges[i].ToVertex == sender) || (Edges[i].FromVertex == sender && Edges[i].ToVertex == _mergeWith))
                {
                    edgesToDel.Add(i);
                }
            }

            foreach (var i in edgesToDel)
            {
                Edges.RemoveAt(i);
            }
            RemoveVertex(true);


            GraphCore.MergeVertices(sender.Number, _mergeWith.Number);
        }

        public void SelectEdge(Edge sender, MouseButtonEventArgs args)
        {
            if (args.RightButton == MouseButtonState.Pressed)
            {
                LastSelected = sender;
            }
        }
        public void VertexStopDrag(Vertex sender)
        {
            if (sender != null)
                sender.IsDragging = false;
        }
        public void VertexDrag(FrameworkElement obj, Vertex sender, MouseEventArgs args)
        {

            if (sender.IsDragging)
            {
                var contentPresenter = VisualTreeHelper.GetParent(obj);
                var canvas = VisualTreeHelper.GetParent(contentPresenter);

                sender.X = args.GetPosition((FrameworkElement)canvas).X - 10;
                sender.Y = args.GetPosition((FrameworkElement)canvas).Y - 10;
            }
        }

        public virtual void ShowTools(FrameworkElement sender, MouseEventArgs args)
        {
            if (args.RightButton != MouseButtonState.Pressed)
            {
                VertexTools.ToolVisibility = EdgeTools.ToolVisibility = CanvasTools.ToolVisibility = Visibility.Collapsed;
                return;
            }
            if (args.OriginalSource is Canvas)
            {
                VertexTools.ToolVisibility = EdgeTools.ToolVisibility = Visibility.Collapsed;

                if (CanvasTools.ToolVisibility == Visibility.Visible)
                {
                    CanvasTools.ToolVisibility = Visibility.Collapsed;
                }
                else
                {
                    CanvasTools.X = args.GetPosition(sender).X + CanvasTools.OffsetX;
                    CanvasTools.Y = args.GetPosition(sender).Y + CanvasTools.OffsetY;
                    CanvasTools.ToolVisibility = Visibility.Visible;
                }

            }
            if (args.OriginalSource is Ellipse)
            {
                CanvasTools.ToolVisibility = EdgeTools.ToolVisibility = Visibility.Collapsed;
                if (VertexTools.ToolVisibility == Visibility.Visible)
                {
                    VertexTools.ToolVisibility = Visibility.Collapsed;
                }
                else
                {
                    VertexTools.X = args.GetPosition(sender).X + VertexTools.OffsetX;
                    VertexTools.Y = args.GetPosition(sender).Y + VertexTools.OffsetY;
                    VertexTools.ToolVisibility = Visibility.Visible;
                }
            }
            if (args.OriginalSource is Line)
            {
                CanvasTools.ToolVisibility = VertexTools.ToolVisibility = Visibility.Collapsed;
                if (EdgeTools.ToolVisibility == Visibility.Visible)
                {
                    EdgeTools.ToolVisibility = Visibility.Collapsed;
                }
                else
                {
                    EdgeTools.X = args.GetPosition(sender).X + EdgeTools.OffsetX;
                    EdgeTools.Y = args.GetPosition(sender).Y + EdgeTools.OffsetY;
                    EdgeTools.ToolVisibility = Visibility.Visible;
                }
            }

        }

        public void AddVertex()
        {
            AddToHistory();
            CanvasTools.ToolVisibility = Visibility.Collapsed;
            Vertices.Add(new Vertex { X = CanvasTools.X - CanvasTools.OffsetX, Y = CanvasTools.Y - CanvasTools.OffsetY, Number = Vertices.Count });
            GraphCore.AddVertex();
            GraphChanged();
        }

        private void GraphChanged()
        {
            switch (_graphRepresentationName)
            {
                case GraphRepresentations.AdjacencyList:
                    GraphRepresentationString = GraphCore.AdjacencyList.ToString();
                    break;
                case GraphRepresentations.AdjacencyMatrix:
                    GraphRepresentationString = GraphCore.AdjacencyMatrix.ToString();
                    break;
                case GraphRepresentations.IncidencyMatrix:
                    GraphRepresentationString = GraphCore.IncidencyMatrix.ToString();
                    break;
                default:
                    GraphRepresentationString = "";
                    break;
            }
        }

        public void RemoveVertex(bool skip = false)
        {
            List<Edge> edgesToRemove = new List<Edge>();
            if (LastSelected is Vertex)
            {
                AddToHistory();
                edgesToRemove.AddRange(Edges.Where(edge => edge.FromVertex == LastSelected || edge.ToVertex == LastSelected));
                foreach (var i in edgesToRemove)
                {
                    Edges.RemoveRange(edgesToRemove);
                }
                if (!skip)
                {
                    GraphCore.RemoveVertex(((Vertex)LastSelected).Number);
                }
                Vertices.Remove((Vertex)LastSelected);
                UpdateVertexNumeration();
                LastSelected = null;
            }

            VertexTools.ToolVisibility = Visibility.Collapsed;
            GraphChanged();
        }

        private void UpdateVertexNumeration()
        {
            for (int i = 0; i < Vertices.Count; i++)
            {
                Vertices[i].Number = i;
            }
        }

        public void RemoveEdge()
        {
            if (LastSelected is Edge)
            {
                AddToHistory();
                Edges.Remove((Edge)LastSelected);
                GraphCore.RemoveEdge(((Edge)LastSelected).FromVertex.Number, ((Edge)LastSelected).ToVertex.Number);
                LastSelected = null;
            }
            EdgeTools.ToolVisibility = Visibility.Collapsed;
            GraphChanged();
        }

        public void SwitchDisplay(string val)
        {
            switch (val)
            {
                case "am":
                    DisplayVisibility = Visibility.Visible;
                    GraphRepresentationName = Enum.GetName(typeof(GraphRepresentations), GraphRepresentations.AdjacencyMatrix);
                    break;

                case "al":
                    DisplayVisibility = Visibility.Visible;
                    GraphRepresentationName = Enum.GetName(typeof(GraphRepresentations), GraphRepresentations.AdjacencyList);
                    break;

                case "im":
                    DisplayVisibility = Visibility.Visible;
                    GraphRepresentationName = Enum.GetName(typeof(GraphRepresentations), GraphRepresentations.IncidencyMatrix);
                    break;
                default:
                    DisplayVisibility = Visibility.Collapsed;
                    GraphRepresentationName = Enum.GetName(typeof(GraphRepresentations), GraphRepresentations.None);
                    break;
            }
            GraphChanged();

        }

        [OnDeserialized]
        internal void OnDeserialized(StreamingContext context)
        {
            GraphCore.AddVertex(Vertices.Count);
            foreach (var edge in Edges)
            {
                GraphCore.AddEdge(edge.FromVertex.Number, edge.ToVertex.Number);
            }
        }
    }
}
