﻿using System.Collections.Generic;

namespace WpfClient.ViewModels
{
    public class DialogInputViewModel : DialogViewModel
    {
        private string _input;
        private List<string> _options;

        public DialogInputViewModel(string message, List<string> inputOptions = null) : base(message)
        {
            Options = inputOptions;
        }

        public string Input
        {
            get { return _input; }
            set { _input = value; NotifyOfPropertyChange(); }
        }

        public List<string> Options
        {
            get { return _options; }
            set { _options = value; NotifyOfPropertyChange(); }
        }

        public void Confirm()
        {
            TryClose(true);
        }

        public void Cancel()
        {
            TryClose(false);
        }
    }
}
