﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using Caliburn.Micro;
using Grafy;
using Grafy.Functions;
using Grafy.Interface;
using Grafy.Representations;
using Microsoft.Win32;

namespace WpfClient.ViewModels
{
    public class ShellViewModel : Conductor<IScreen>.Collection.OneActive, IHandle<string>
    {
        private IEventAggregator _ev;
        private IWindowManager _wm;

        private bool _canBack = false;
        private bool _canEuler = false;
        private bool _canMst = false;
        private bool _canDist = false;
        private bool _canSearch = false;
        private bool _canComp = false;

        public ShellViewModel()
        {
            _ev = IoC.Get<IEventAggregator>("ev");
            _ev.Subscribe(this);
            _wm = IoC.Get<IWindowManager>("wm");
        }

        public bool CanBack
        {
            get { return _canBack; }
            set { _canBack = value; NotifyOfPropertyChange(); }
        }

        public bool CanEuler
        {
            get { return _canEuler; }
            set { _canEuler = value; NotifyOfPropertyChange(); }
        }

        public bool CanMst
        {
            get { return _canMst; }
            set { _canMst = value; NotifyOfPropertyChange(); }
        }

        public bool CanDist
        {
            get { return _canDist; }
            set { _canDist = value; NotifyOfPropertyChange(); }
        }

        public bool CanSearch
        {
            get { return _canSearch; }
            set { _canSearch = value; NotifyOfPropertyChange(); }
        }

        public bool CanComp
        {
            get { return _canComp; }
            set { _canComp = value; NotifyOfPropertyChange(); }
        }

        public void Back()
        {
            if (ActiveItem is CanvasViewModel)
                ((CanvasViewModel)ActiveItem)?.HistoryBack();
            if (ActiveItem is OrientedCanvasViewModel)
                ((OrientedCanvasViewModel)ActiveItem)?.HistoryBack();
            if (ActiveItem is WeightedCanvasViewModel)
                ((WeightedCanvasViewModel)ActiveItem)?.HistoryBack();
            if (ActiveItem is WeightedOrientedCanvasViewModel)
                ((WeightedOrientedCanvasViewModel)ActiveItem)?.HistoryBack();
        }

        public void SwitchDisplay(string val)
        {
            if (ActiveItem is CanvasViewModel)
                ((CanvasViewModel)ActiveItem)?.SwitchDisplay(val);
            if (ActiveItem is OrientedCanvasViewModel)
                ((OrientedCanvasViewModel)ActiveItem)?.SwitchDisplay(val);
            if (ActiveItem is WeightedCanvasViewModel)
                ((WeightedCanvasViewModel)ActiveItem)?.SwitchDisplay(val);
            if (ActiveItem is WeightedOrientedCanvasViewModel)
                ((WeightedOrientedCanvasViewModel)ActiveItem)?.SwitchDisplay(val);
        }

        public void Handle(string message)
        {
            _wm.ShowDialog(new DialogViewModel(message));
        }

        public async void ShowDFS()
        {
            dynamic settings = new ExpandoObject();
            settings.Title = "DFS Search";
            settings.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            List<string> list = new List<string>();

            if (ActiveItem is CanvasViewModel)
                list = ((CanvasViewModel)ActiveItem).Vertices.Select(s => s.Number.ToString()).ToList();

            if (ActiveItem is OrientedCanvasViewModel)
                list = ((OrientedCanvasViewModel)ActiveItem).Vertices.Select(s => s.Number.ToString()).ToList();

            if (ActiveItem is WeightedCanvasViewModel)
                list = ((WeightedCanvasViewModel)ActiveItem).Vertices.Select(s => s.Number.ToString()).ToList();

            if (ActiveItem is WeightedOrientedCanvasViewModel)
                list = ((WeightedOrientedCanvasViewModel)ActiveItem).Vertices.Select(s => s.Number.ToString()).ToList();

            var dialog = new DialogInputViewModel("Select starting point", list);

            var result = _wm.ShowDialog(dialog, null, settings);

            if (result)
            {
                dynamic windowsettings = new ExpandoObject();
                windowsettings.WindowStyle = WindowStyle.SingleBorderWindow;
                windowsettings.IsHitTestVisible = false;
                windowsettings.Title = "DFS Result";
                windowsettings.MinWidth = 300;
                windowsettings.MinHeight = 300;
                windowsettings.WindowStartupLocation = WindowStartupLocation.CenterScreen;


                if (ActiveItem is CanvasViewModel)
                {
                    var path = await ((CanvasViewModel)ActiveItem).GraphCore.SearchAsync(int.Parse(dialog.Input), SearchType.DFS);
                    _wm.ShowWindow(new CanvasViewModel((CanvasViewModel)ActiveItem, path), null, windowsettings);
                }
                if (ActiveItem is OrientedCanvasViewModel)
                {
                    var path = await ((OrientedCanvasViewModel)ActiveItem).GraphCore.SearchAsync(int.Parse(dialog.Input), SearchType.DFS);
                    _wm.ShowWindow(new OrientedCanvasViewModel((OrientedCanvasViewModel)ActiveItem, path), null, windowsettings);
                }

                if (ActiveItem is WeightedCanvasViewModel)
                {
                    var path = await ((WeightedCanvasViewModel)ActiveItem).GraphCore.SearchAsync(int.Parse(dialog.Input), SearchType.DFS);
                    _wm.ShowWindow(new WeightedCanvasViewModel((WeightedCanvasViewModel)ActiveItem, path), null, windowsettings);
                }

                if (ActiveItem is WeightedOrientedCanvasViewModel)
                {
                    var path = await ((WeightedOrientedCanvasViewModel)ActiveItem).GraphCore.SearchAsync(int.Parse(dialog.Input), SearchType.DFS);
                    _wm.ShowWindow(new WeightedOrientedCanvasViewModel((WeightedOrientedCanvasViewModel)ActiveItem, path), null, windowsettings);
                }
            }
        }

        public async void ShowBFS()
        {
            dynamic settings = new ExpandoObject();
            settings.Title = "BFS Search";
            settings.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            List<string> list = new List<string>();

            if (ActiveItem is CanvasViewModel)
                list = ((CanvasViewModel)ActiveItem).Vertices.Select(s => s.Number.ToString()).ToList();
            if (ActiveItem is OrientedCanvasViewModel)
                list = ((OrientedCanvasViewModel)ActiveItem).Vertices.Select(s => s.Number.ToString()).ToList();
            if (ActiveItem is WeightedCanvasViewModel)
                list = ((WeightedCanvasViewModel)ActiveItem).Vertices.Select(s => s.Number.ToString()).ToList();
            if (ActiveItem is WeightedOrientedCanvasViewModel)
                list = ((WeightedOrientedCanvasViewModel)ActiveItem).Vertices.Select(s => s.Number.ToString()).ToList();

            var dialog = new DialogInputViewModel("Select starting point", list);

            var result = _wm.ShowDialog(dialog, null, settings);

            if (result)
            {
                dynamic windowsettings = new ExpandoObject();
                windowsettings.WindowStyle = WindowStyle.SingleBorderWindow;
                windowsettings.IsHitTestVisible = false;
                windowsettings.Title = "BFS Result";
                windowsettings.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                windowsettings.MinWidth = 300;
                windowsettings.MinHeight = 300;
                if (ActiveItem is CanvasViewModel)
                {
                    var path = await ((CanvasViewModel)ActiveItem).GraphCore.SearchAsync(int.Parse(dialog.Input), SearchType.BFS);
                    _wm.ShowWindow(new CanvasViewModel((CanvasViewModel)ActiveItem, path), null, windowsettings);
                }

                if (ActiveItem is OrientedCanvasViewModel)
                {
                    var path = await ((OrientedCanvasViewModel)ActiveItem).GraphCore.SearchAsync(int.Parse(dialog.Input), SearchType.BFS);
                    _wm.ShowWindow(new OrientedCanvasViewModel((OrientedCanvasViewModel)ActiveItem, path), null, windowsettings);
                }
                if (ActiveItem is WeightedCanvasViewModel)
                {
                    var path = await ((WeightedCanvasViewModel)ActiveItem).GraphCore.SearchAsync(int.Parse(dialog.Input), SearchType.BFS);
                    _wm.ShowWindow(new WeightedCanvasViewModel((WeightedCanvasViewModel)ActiveItem, path), null, windowsettings);
                }
                if (ActiveItem is WeightedOrientedCanvasViewModel)
                {
                    var path = await ((WeightedOrientedCanvasViewModel)ActiveItem).GraphCore.SearchAsync(int.Parse(dialog.Input), SearchType.BFS);
                    _wm.ShowWindow(new WeightedOrientedCanvasViewModel((WeightedOrientedCanvasViewModel)ActiveItem, path), null, windowsettings);
                }

            }
        }

        public async void ShowIsConnected()
        {
            dynamic settings = new ExpandoObject();
            settings.Title = "Graph connectivity";
            settings.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            bool connected = false;
            if (ActiveItem is CanvasViewModel)
            {
                connected = await ((CanvasViewModel)ActiveItem).GraphCore.IsConnected();

            }
            if (ActiveItem is OrientedCanvasViewModel)
            {
                connected = await ((OrientedCanvasViewModel)ActiveItem).GraphCore.IsConnected();

            }
            if (ActiveItem is WeightedCanvasViewModel)
            {
                connected = await ((WeightedCanvasViewModel)ActiveItem).GraphCore.IsConnected();
            }
            if (ActiveItem is WeightedOrientedCanvasViewModel)
            {
                connected = await ((WeightedOrientedCanvasViewModel)ActiveItem).GraphCore.IsConnected();
            }
            _wm.ShowDialog(new DialogViewModel(connected ? "Graph is connected" : "Graph isn't connected"), null, settings);

        }

        public async void ShowComponents()
        {
            dynamic settings = new ExpandoObject();
            settings.Title = "Graph components";

            int components = 0;
            if (ActiveItem is CanvasViewModel)
            {
                components = await ((CanvasViewModel)ActiveItem).GraphCore.Components();
            }
            if (ActiveItem is OrientedCanvasViewModel)
            {
                components = await ((OrientedCanvasViewModel)ActiveItem).GraphCore.Components();
            }
            if (ActiveItem is WeightedCanvasViewModel)
            {
                components = await ((WeightedCanvasViewModel)ActiveItem).GraphCore.Components();
            }
            if (ActiveItem is WeightedOrientedCanvasViewModel)
            {
                components = await ((WeightedOrientedCanvasViewModel)ActiveItem).GraphCore.Components();
            }
            _wm.ShowDialog(new DialogViewModel($"Graph has {components} components"), null, settings);


        }

        public void ShowEuler()
        {
            dynamic settings = new ExpandoObject();
            settings.Title = "Euler";
            settings.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            Euler euler = null;
            if (ActiveItem is CanvasViewModel)
            {
                euler = new Euler(((CanvasViewModel)ActiveItem).GraphCore.AdjacencyMatrix);
            }
            if (ActiveItem is OrientedCanvasViewModel)
            {
                euler = new Euler(((OrientedCanvasViewModel)ActiveItem).GraphCore.AdjacencyMatrix);
            }
            if (ActiveItem is WeightedCanvasViewModel)
            {
                euler = new Euler(((WeightedCanvasViewModel)ActiveItem).GraphCore.AdjacencyMatrix);
            }
            if (ActiveItem is WeightedOrientedCanvasViewModel)
            {
                euler = new Euler(((WeightedOrientedCanvasViewModel)ActiveItem).GraphCore.AdjacencyMatrix);
            }

            var result = euler.FindEulerCircuit();
            if (result.Any())
            {
                result.Reverse();
                _wm.ShowDialog(
                    new DialogViewModel(
                        $"Euler cycle: {result.Aggregate("", (current, i) => current + (i + ", "))}"), null, settings);
            }
            else
            {
                _wm.ShowDialog(new DialogViewModel($"Euler cycle does not exist"), null, settings);
            }
        }

        public void ShowRadius()
        {
            dynamic settings = new ExpandoObject();
            settings.Title = "Radius";
            settings.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            int rad = 0;
            if (ActiveItem is WeightedCanvasViewModel)
            {
                rad = ((WeightedCanvasViewModel)ActiveItem).GraphCore.Radius;
            }
            if (ActiveItem is WeightedOrientedCanvasViewModel)
            {
                rad = ((WeightedOrientedCanvasViewModel)ActiveItem).GraphCore.Radius;
            }
            _wm.ShowDialog(new DialogViewModel($"Radius: {rad}"), null, settings);

        }
        public void ShowDiameter()
        {
            dynamic settings = new ExpandoObject();
            settings.Title = "Diameter";
            settings.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            int diam = 0;
            if (ActiveItem is WeightedCanvasViewModel)
            {
                diam = ((WeightedCanvasViewModel)ActiveItem).GraphCore.Diameter;
            }
            if (ActiveItem is WeightedOrientedCanvasViewModel)
            {
                diam = ((WeightedOrientedCanvasViewModel)ActiveItem).GraphCore.Diameter;
            }
            _wm.ShowDialog(new DialogViewModel($"Diameter: {diam}"), null, settings);
        }
        public void ShowCentral()
        {
            dynamic settings = new ExpandoObject();
            settings.Title = "Central Nodes";
            settings.Height = 400;
            settings.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            List<int> centralv = new List<int>();
            if (ActiveItem is WeightedCanvasViewModel)
            {
                centralv = ((WeightedCanvasViewModel)ActiveItem).GraphCore.CentralVertices;
            }
            if (ActiveItem is WeightedOrientedCanvasViewModel)
            {
                centralv = ((WeightedOrientedCanvasViewModel)ActiveItem).GraphCore.CentralVertices;
            }
            if (centralv.Any())
            {
                _wm.ShowDialog(new DialogViewModel($"Central Nodes: {centralv.Aggregate("", (current, i) => current + (i + ", "))}"), null, settings);
            }
        }

        public void ShowDistance()
        {
            dynamic settings = new ExpandoObject();
            settings.Title = "Distance";
            settings.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            List<string> nodes = new List<string>();

            if (ActiveItem is WeightedCanvasViewModel)
                nodes = ((WeightedCanvasViewModel)ActiveItem).Vertices.Select(v => v.Number.ToString()).ToList();

            if (ActiveItem is WeightedOrientedCanvasViewModel)
                nodes = ((WeightedOrientedCanvasViewModel)ActiveItem).Vertices.Select(v => v.Number.ToString()).ToList();


            var input1 = new DialogInputViewModel("From vertex", nodes);

            var result1 = _wm.ShowDialog(input1, null, settings);

            if (!result1) return;

            nodes.Remove(input1.Input);
            var input2 = new DialogInputViewModel("To vertex", nodes);

            var result2 = _wm.ShowDialog(input2, null, settings);
            if (result2)
            {
                int dist = 0;


                if (ActiveItem is WeightedCanvasViewModel)
                    dist = ((WeightedCanvasViewModel)ActiveItem).GraphCore.DistanceBetweenVertices(
                    int.Parse(input1.Input), int.Parse(input2.Input));

                if (ActiveItem is WeightedOrientedCanvasViewModel)
                    dist = ((WeightedOrientedCanvasViewModel)ActiveItem).GraphCore.DistanceBetweenVertices(
                    int.Parse(input1.Input), int.Parse(input2.Input));

                _wm.ShowDialog(new DialogViewModel($"Distance between {input1.Input} and {input2.Input} is {dist}"),
                    null, settings);
            }
        }

        public async void ShowMST()
        {
            dynamic settings = new ExpandoObject();
            settings.Title = "MST";
            settings.WindowStartupLocation = WindowStartupLocation.CenterScreen;


            if (ActiveItem is WeightedCanvasViewModel)
            {
                var dialog = new DialogInputViewModel("Select starting vertex",
                    ((WeightedCanvasViewModel)ActiveItem).Vertices.Select(v => v.Number.ToString()).ToList());
                var result = _wm.ShowDialog(dialog, null, settings);
                if (result)
                {
                    WeightedAdjacencyMatrix mst;
                    try
                    {
                        mst = await MST.Prim(((WeightedCanvasViewModel)ActiveItem).GraphCore.AdjacencyMatrix,
                            int.Parse(dialog.Input));
                    }
                    catch (ArgumentException)
                    {
                        _wm.ShowDialog(new DialogViewModel("Graph is not connected"), null, settings);
                        return;
                    }
                    //_wm.ShowDialog(new DialogViewModel(mst.ToString()), null, settings);
                    settings.IsHitTestVisible = false;
                    settings.MinWidth = 500;
                    settings.MinHeight = 500;
                    _wm.ShowWindow(new WeightedCanvasViewModel((WeightedCanvasViewModel)ActiveItem, mst), null, settings);

                }

            }
            if (ActiveItem is WeightedOrientedCanvasViewModel)
            {
                var dialog = new DialogInputViewModel("Select starting vertex",
                    ((WeightedOrientedCanvasViewModel)ActiveItem).Vertices.Select(v => v.Number.ToString()).ToList());
                var result = _wm.ShowDialog(dialog, null, settings);
                if (result)
                {
                    WeightedOrientedAdjacencyMatrix mst;
                    try
                    {
                        mst = await MST.Prim(((WeightedOrientedCanvasViewModel)ActiveItem).GraphCore.AdjacencyMatrix,
                            int.Parse(dialog.Input));
                    }
                    catch (ArgumentException)
                    {
                        _wm.ShowDialog(new DialogViewModel("Graph is not connected"), null, settings);
                        return;
                    }
                    settings.IsHitTestVisible = false;
                    settings.MinWidth = 500;
                    settings.MinHeight = 500;
                    _wm.ShowWindow(new WeightedOrientedCanvasViewModel((WeightedOrientedCanvasViewModel)ActiveItem,mst),null,settings);
                    //_wm.ShowDialog(new DialogViewModel(mst.ToString()), null, settings);

                }

            }

        }

        public void NewNormal()
        {
            ActivateItem(new CanvasViewModel());
            ((CanvasViewModel)ActiveItem).History.CollectionChanged += (sender, args) =>
            {
                CanBack = ((CanvasViewModel)ActiveItem).History.Count > 0;
            };
        }

        public void NewOriented()
        {
            ActivateItem(new OrientedCanvasViewModel());
            ((OrientedCanvasViewModel)ActiveItem).History.CollectionChanged += (sender, args) =>
            {
                CanBack = ((OrientedCanvasViewModel)ActiveItem).History.Count > 0;
            };
        }

        public void NewWeighted()
        {
            ActivateItem(new WeightedCanvasViewModel());
            ((WeightedCanvasViewModel)ActiveItem).History.CollectionChanged += (sender, args) =>
            {
                CanBack = ((WeightedCanvasViewModel)ActiveItem).History.Count > 0;
            };
        }

        public void NewWeightedOriented()
        {
            ActivateItem(new WeightedOrientedCanvasViewModel());
            ((WeightedOrientedCanvasViewModel)ActiveItem).History.CollectionChanged += (sender, args) =>
            {
                CanBack = ((WeightedOrientedCanvasViewModel)ActiveItem).History.Count > 0;
            };
        }

        public async void Load()
        {
            var dialog = new OpenFileDialog()
            {
                CheckPathExists = true,
                CheckFileExists = true,
                AddExtension = true,
                Filter = "All Graphs (*.normal, *.oriented,*.weighted,*.weightedoriented)|*.normal;*.oriented;*.weighted;*.weightedoriented|" +
                         "Normal Graph (*.normal)|*.normal|" +
                         "Oriented Graph (*.oriented)|*.oriented|" +
                         "Weighted Graph (*.weighted)|*.weighted|" +
                         "Weighted & Oriented Graph (*.weightedoriented)|(*.weightedoriented)",
                ValidateNames = true,
                InitialDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            };
            var dialogResult = dialog.ShowDialog();
            if (dialogResult != null && dialogResult.Value)
            {
                var ext = Path.GetExtension(dialog.FileName);
                if (ext == ".normal")
                {
                    ActivateItem(new CanvasViewModel(await Json.IO.ReadFromJsonFile<CanvasViewModel>(dialog.FileName)));
                }
                if (ext == ".oriented")
                {
                    ActivateItem(new OrientedCanvasViewModel(await Json.IO.ReadFromJsonFile<OrientedCanvasViewModel>(dialog.FileName)));
                }
                if (ext == ".weighted")
                {
                    ActivateItem(new WeightedCanvasViewModel(await Json.IO.ReadFromJsonFile<WeightedCanvasViewModel>(dialog.FileName)));
                }
                if (ext == ".weightedoriented")
                {
                    ActivateItem(new WeightedOrientedCanvasViewModel(await Json.IO.ReadFromJsonFile<WeightedOrientedCanvasViewModel>(dialog.FileName)));
                }
                ActiveItem.DisplayName = Path.GetFileName(dialog.FileName);
            }

        }

        public void Save()
        {
            if (ActiveItem == null) return;
            var dialog = new SaveFileDialog
            {
                CheckPathExists = true,
                AddExtension = true,
                ValidateNames = true,
                InitialDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            };
            if (ActiveItem is CanvasViewModel)
            {
                dialog.DefaultExt = "normal";
                dialog.Filter = "Normal Graph(*.normal)|*.normal";
            }
            if (ActiveItem is OrientedCanvasViewModel)
            {
                dialog.DefaultExt = "oriented";
                dialog.Filter = "Oriented Graph(*.oriented)|*.oriented";
            }
            if (ActiveItem is WeightedCanvasViewModel)
            {
                dialog.DefaultExt = "weighted";
                dialog.Filter = "Weighted Graph(*.weighted)|*.weighted";
            }
            if (ActiveItem is WeightedOrientedCanvasViewModel)
            {
                dialog.DefaultExt = "weightedoriented";
                dialog.Filter = "Weighted & Oriented Graph (*.weightedoriented)|(*.weightedoriented)";
            }


            var dialogResult = dialog.ShowDialog();
            if (dialogResult != null && dialogResult.Value)
            {
                var ext = Path.GetExtension(dialog.FileName);
                if (ext == ".normal")
                {
                    Json.IO.WriteToJsonFile(dialog.FileName, (CanvasViewModel)ActiveItem);
                }
                if (ext == ".oriented")
                {
                    Json.IO.WriteToJsonFile(dialog.FileName, (OrientedCanvasViewModel)ActiveItem);
                }
                if (ext == ".weighted")
                {
                    Json.IO.WriteToJsonFile(dialog.FileName, (WeightedCanvasViewModel)ActiveItem);
                }
                if (ext == ".weightedoriented")
                {
                    Json.IO.WriteToJsonFile(dialog.FileName, (WeightedOrientedCanvasViewModel)ActiveItem);
                }
            }
            ActiveItem.DisplayName = Path.GetFileName(dialog.FileName);
        }

        public void Close()
        {
            ActiveItem.TryClose();
            if (!Items.Any())
            {
                CanBack = false;
            }
        }
        public void Exit()
        {
            Application.Current.MainWindow.Close();
        }

        public void TabChanged()
        {

            if (ActiveItem is CanvasViewModel)
            {
                CanBack = ((CanvasViewModel)ActiveItem)?.History.Count > 0;
                CanSearch = true;
                CanComp = true;
                CanEuler = true;

                CanDist = false;
                CanMst = false;
            }
            if (ActiveItem is OrientedCanvasViewModel)
            {
                CanBack = ((OrientedCanvasViewModel)ActiveItem)?.History.Count > 0;
                CanSearch = true;
                CanComp = true;
                CanEuler = true;

                CanDist = false;
                CanMst = false;
            }
            if (ActiveItem is WeightedCanvasViewModel)
            {
                CanBack = ((WeightedCanvasViewModel)ActiveItem)?.History.Count > 0;
                CanSearch = true;
                CanComp = true;
                CanDist = true;
                CanEuler = true;
                CanMst = true;
            }
            if (ActiveItem is WeightedOrientedCanvasViewModel)
            {
                CanBack = ((WeightedOrientedCanvasViewModel)ActiveItem)?.History.Count > 0;
                CanSearch = true;
                CanComp = true;
                CanDist = true;
                CanEuler = true;
                CanMst = true;
            }
        }
    }
}
