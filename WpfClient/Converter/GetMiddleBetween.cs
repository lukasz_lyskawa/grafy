﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace WpfClient.Converter
{
    public class GetMiddleBetween : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var val1 = values[0] as double? ?? 0;
            var val2 = values[1] as double? ?? 0;

            if (val1 < val2)
            {
                return val1 + (val2-val1)/2;
            }
            else
            {
                return val2 + (val1-val2)/2;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
