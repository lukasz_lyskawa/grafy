﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using WpfClient.Models;

namespace WpfClient.Converter
{
    public class VertexToPointConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var v = (Vertex) value;
         

            var len = Math.Sqrt(v.X*v.X + v.Y*v.Y);
            return new Point(v.X/len,v.Y/len);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
