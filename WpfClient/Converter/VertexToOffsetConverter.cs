﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using WpfClient.Models;

namespace WpfClient.Converter
{
    public class VertexToOffsetConverter : IMultiValueConverter
    {


        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var x1 = values[0] as double? ?? 0;
            var y1 = values[1] as double? ?? 0;
            var x2 = values[2] as double? ?? 0;
            var y2 = values[3] as double? ?? 0;

            double x;
            if (x1 > x2)
            {
                x= 1;
            }
            else
            {
                x= 0;
            }
            double y;
            if (y1 > y2)
            {
                y = 1;
            }
            else
            {
                y = 0;
            }
            return new Point(x,y);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
