﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Json
{
    public static class IO
    {
        public static async void WriteToJsonFile<T>(string path, T inputObject, bool append = false) where T : new()
        {
            TextWriter writer = null;
            try
            {
                var settings = new JsonSerializerSettings {ContractResolver = new ContractResolver()};
                var content = JsonConvert.SerializeObject(inputObject);
                writer = new StreamWriter(path, append);
                await writer.WriteAsync(content);
            }
            finally
            {
                writer?.Close();
            }
        }

        public static async Task<T> ReadFromJsonFile<T>(string path) where T : new()
        {
            TextReader reader = null;
            try
            {
                reader = new StreamReader(path);
                var content = await reader.ReadToEndAsync();
                var settings = new JsonSerializerSettings { ContractResolver = new ContractResolver() };
                return JsonConvert.DeserializeObject<T>(content);
            }
            finally
            {
                reader?.Close();
            }
        }
    }

    public class ContractResolver : DefaultContractResolver
    {
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            var props = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                        .Select(p => base.CreateProperty(p, memberSerialization))
                    .Union(type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                               .Select(f => base.CreateProperty(f, memberSerialization)))
                    .ToList();
            props.ForEach(p => { p.Writable = true; p.Readable = true; });
            return props;
        }
    }
}
