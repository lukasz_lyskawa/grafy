﻿using System.Windows;
using System.Windows.Controls;

namespace SharedUserControls
{
    /// <summary>
    /// Interaction logic for Toolbar.xaml
    /// </summary>
    public partial class Toolbar : UserControl
    {
        public Toolbar()
        {
            InitializeComponent();
        }

        public void Minimize_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            Window.GetWindow(this).WindowState = WindowState.Minimized;
        }
        public void Maximize_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            Window.GetWindow(this).WindowState = Window.GetWindow(this).WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized;
        }

        public void Close_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            Application.Current.Shutdown();
        }
    }
}
